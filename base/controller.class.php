<?php
/*
 * controller 基类
 * 
 */
class controller extends base{

	function __construct(){
		parent::__construct();
	}
	final protected function load_model($model_name){
		if(!file_exists(MODEL_PATH.$model_name.".class.php")){
			new Exception('model not found');
			die();
		}
		require MODEL_PATH.$model_name.".class.php";
	}
	
}
