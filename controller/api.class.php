<?php
class api extends controller {
	public function __construct() {
		parent::__construct ();
	}
	public function default_action() {
		//
		
	}
	
	/**
	 * 
	 * 计算并返回合适的带宽数值
	 *
	 * @param int $rate 带宽值，单位bytes
	 * 
	 * @return string 用来显示带宽的字符串
	 */
	private function get_rate_string($rate){
		if($rate<1024){
			return number_format($rate,2,'.','')." Bytes";			
		}		
		$rate=$rate/1024;		
		if($rate<1024){
			
			return number_format($rate,2,'.','')." KBytes";
		}
		$rate=$rate/1024;
		if($rate<1024){
			return number_format($rate,2,'.','')." MBytes";
		}
		$rate=$rate/1024;
		if($rate<1024){
			return number_format($rate,2,'.','')." GBytes";
		}
		$rate=$rate/1024;
		if($rate<1024){
			return number_format($rate,2,'.','')." TBytes";
		}
		$rate=$rate/1024;
		if($rate<1024){
			return number_format($rate,2,'.','')." PBytes";
		}
		
		
		
		
	}
	public function indexdata() {
		$indexdata=unserialize($this->redis->get('indexdata'));
		if((!$indexdata) || ($indexdata['logtime']+60*5<$this->timestamp)){
			$api=new psapi($this->CONFIG['api']['PID'], $this->CONFIG['api']['PK'], $this->CONFIG['api']['as_api']);
			$arr=$api->ASQU();
			$OLCM=$arr['OLACM'];
			$OLCL=$arr['OLACL'];
			$bw=$arr['TXABS']+$arr['RXABS'];
			if(!$indexdata){
				$RATE='Unknow';
			}else{
				$RATE=abs($bw-$indexdata['bw']);
				$RATE=$RATE/($this->timestamp-$indexdata['logtime']);
				$RATE=$this->get_rate_string($RATE)."/s";
			}

			$ret=array(
				'OLCM'=>$OLCM,
				'OLCL'=>$OLCL,
				'RATE'=>$RATE,
			);
			
			$indexdata=array(
				'indexdata'=>$ret,
				'logtime'=>$this->timestamp,
				'bw'=>$bw,
			);
			
			$this->redis->set('indexdata',serialize($indexdata));
		}
		
		$retarr=$indexdata['indexdata'];
		echo json_encode ( $retarr );
		return;
	}
}