<?php
/*
 * 
 * model 基类
 * 
 * 
 */

class model extends base{
	protected $db = null;
	
	function __construct(){
		parent::__construct();
		$this->db_init();
	}
	
	private function db_init(){
		$this->db=self::get_db_connect();
		
		return;
	}
	
	protected static function get_db_connect(){
		$config=Application::get_config();
		$db = new ezSQL_mysql($config['database']['db_user'],$config['database']['db_password'],$config['database']['db_name'],$config['database']['db_host']);
		return $db;
		
	}
}

