<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>图表测试</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'line',
                marginRight: 130,
                marginBottom: 25
            },
            title: {
                text: '在店人数',
                x: -20 //center
            },
            subtitle: {
                text: '此时此刻在店使用Joome Free WiFi的用户数量',
                x: -20
            },
            xAxis: {
                categories: ['9:00', '10:00', '11:00', '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']
            },
            yAxis: {
                title: {
                    text: '人数量'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'人';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: '西单店',
                data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
            }, {
                name: '三里屯店',
                data: [2, 8, 5, 11, 17, 22, 24, 24, 20, 14, 8, 2]
            }, {
                name: '后海店',
                data: [9, 6, 3, 8, 13, 17, 18, 17, 14, 9, 3, 1]
            }, {
                name: '798店',
                data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
            }]
        });
    });
    
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

	</body>
</html>
