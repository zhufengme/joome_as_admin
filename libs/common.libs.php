<?php
//以下为本项目公共函数库

function get_utc_timestamp(){
	return time()-date('Z');
}

function send_http_post($url,$data){
	$curl = curl_init ();		
	curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
	curl_setopt($curl,CURLOPT_URL,$url);
	curl_setopt ( $curl, CURLOPT_POST, 1 );
	curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
	$data=curl_exec($curl);
	return $data;
	
}

function get_rand_str($len) {
	$str = false;
	$arr = array ("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" );
	
	while ( strlen ( $str ) < $len ) {
		$i = rand ( 1, count ( $arr ) - 1 );
		$str .= $arr [$i];
	}
	return $str;
}

function dateadd($time,$type,$value){
	$day=date("d",$time);
	$month=date("m",$time);
	$year=date("Y",$time);
	switch ($type){
		case "m":
			$month=$month+$value;
			break;
		case "d":
			$day=$day+$value;
			break;
		case "Y":
			$year=$year+$value;
			break;
	}
	
	return mktime(0,0,0,$month,$day,$year);
}

//页面跳转
function page_jump($url){
	header("Location: $url");
	die;
}

//发送xmpp通知
function xmpp_message($to,$message){
	$message=mb_convert_encoding($message,"utf-8","gbk");
	$conn=new XMPPHP_XMPP("talk.google.com",5222,"qvbuy.alert@gmail.com","333xiaozhu",'xmpphp', 'gmail.com', $printlog=false, $loglevel=LOGGER_LEVEL_ERROR);
	$conn->connect();
	$conn->processUntil('session_start');
	$conn->message($to, $message);
	$conn->disconnect();
}

/*
        * 中文截取，支持gb2312,gbk,utf-8,big5
        *
        * @param string $str 要截取的字串
        * @param int $start 截取起始位置
        * @param int $length 截取长度
        * @param string $charset utf-8|gb2312|gbk|big5 编码
        * @param $suffix 是否加尾缀
        */
function csubstr($_String, $_Start=0,$_Length,  $_Encode='GBK')
{
 $_String=htmlspecialchars_decode($_String);
 $v = 0;
 if($_Encode == 'UTF-8')
 {
 $_Pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
 preg_match_all($_Pa, $_String, $_Rarray);
 $_SLength = count($_Rarray[0]);
 if($_SLength < $_Length) return $_String;
 for($i=$_Start; $i<$_SLength; $i++)
 {
 if($v >= $_Length * 2 - 1) return $_RS.'..';
 if(ord($_Rarray[0][$i]) > 129) $v += 2;
 else $v++;
 $_RS .= $_Rarray[0][$i];
 }
 } else {
 $_Start = $_Start * 2;
 $_Length = $_Length * 2;
 $_Len = strlen($_String);
 if($_Len < $_Length) return $_String;
 $_Rstring= '';
 for($i=$_Start; $i<$_Len; $i++)
 {
 if($v >= $_Length - 1) return $_Rstring.'..';
 if(ord(substr($_String, $i, 1)) > 129) { $_Rstring .= substr($_String, $i, 2); $v += 2; $i++; }
 else { $_Rstring .= substr($_String, $i, 1); $v++; }
 }
 return $_Rstring.'..';
 }
}



/**
 * 计算字符串的长度（汉字按照两个字符计算）
 *
 * @param   string      $str        字符串
 *
 * @return  int
 */
function str_len($str) {
	$length = strlen ( preg_replace ( '/[\x00-\x7F]/', '', $str ) );
	
	if ($length) {
		return strlen ( $str ) - $length + intval ( $length / 3 ) * 2;
	} else {
		return strlen ( $str );
	}
}
/**
 * 获得用户的真实IP地址
 *
 * @access  public
 * @return  string
 */
function real_ip() {
	return $_SERVER['REMOTE_ADDR'];
	
	static $realip = NULL;
	
	if ($realip !== NULL) {
		return $realip;
	}
	
	if (isset ( $_SERVER )) {
		if (isset ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
			$arr = explode ( ',', $_SERVER ['HTTP_X_FORWARDED_FOR'] );
			
			/* 取X-Forwarded-For中第一个非unknown的有效IP字符串 */
			foreach ( $arr as $ip ) {
				$ip = trim ( $ip );
				
				if ($ip != 'unknown') {
					$realip = $ip;
					
					break;
				}
			}
		} elseif (isset ( $_SERVER ['HTTP_CLIENT_IP'] )) {
			$realip = $_SERVER ['HTTP_CLIENT_IP'];
		} else {
			if (isset ( $_SERVER ['REMOTE_ADDR'] )) {
				$realip = $_SERVER ['REMOTE_ADDR'];
			} else {
				$realip = '0.0.0.0';
			}
		}
	} else {
		if (getenv ( 'HTTP_X_FORWARDED_FOR' )) {
			$realip = getenv ( 'HTTP_X_FORWARDED_FOR' );
		} elseif (getenv ( 'HTTP_CLIENT_IP' )) {
			$realip = getenv ( 'HTTP_CLIENT_IP' );
		} else {
			$realip = getenv ( 'REMOTE_ADDR' );
		}
	}
	
	preg_match ( "/[\d\.]{7,15}/", $realip, $onlineip );
	$realip = ! empty ( $onlineip [0] ) ? $onlineip [0] : '0.0.0.0';
	
	return $realip;
}

/**
 * 验证输入的邮件地址是否合法
 *
 * @access  public
 * @param   string      $email      需要验证的邮件地址
 *
 * @return bool
 */
function is_email($user_email) {
	$chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
	if (strpos ( $user_email, '@' ) !== false && strpos ( $user_email, '.' ) !== false) {
		if (preg_match ( $chars, $user_email )) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
/**
 * 检查是否为一个合法的时间格式
 *
 * @access  public
 * @param   string  $time
 * @return  void
 */
function is_time($time) {
	$pattern = '/[\d]{4}-[\d]{1,2}-[\d]{1,2}\s[\d]{1,2}:[\d]{1,2}:[\d]{1,2}/';
	
	return preg_match ( $pattern, $time );
}
/**
 * 检查目标文件夹是否存在，如果不存在则自动创建该目录
 *
 * @access      public
 * @param       string      folder     目录路径。不能使用相对于网站根目录的URL
 *
 * @return      bool
 */
function make_dir($folder) {
	$reval = false;
	
	if (! file_exists ( $folder )) {
		/* 如果目录不存在则尝试创建该目录 */
		@umask ( 0 );
		
		/* 将目录路径拆分成数组 */
		preg_match_all ( '/([^\/]*)\/?/i', $folder, $atmp );
		
		/* 如果第一个字符为/则当作物理路径处理 */
		$base = ($atmp [0] [0] == '/') ? '/' : '';
		
		/* 遍历包含路径信息的数组 */
		foreach ( $atmp [1] as $val ) {
			if ('' != $val) {
				$base .= $val;
				
				if ('..' == $val || '.' == $val) {
					/* 如果目录为.或者..则直接补/继续下一个循环 */
					$base .= '/';
					
					continue;
				}
			} else {
				continue;
			}
			
			$base .= '/';
			
			if (! file_exists ( $base )) {
				/* 尝试创建目录，如果创建失败则继续循环 */
				if (@mkdir ( rtrim ( $base, '/' ), 0777 )) {
					@chmod ( $base, 0777 );
					$reval = true;
				}
			}
		}
	} else {
		/* 路径已经存在。返回该路径是不是一个目录 */
		$reval = is_dir ( $folder );
	}
	
	clearstatcache ();
	
	return $reval;
}
/**
 * 递归方式的对变量中的特殊字符进行转义
 *
 * @access  public
 * @param   mix     $value
 *
 * @return  mix
 */
function addslashes_deep($value) {
	if (empty ( $value )) {
		return $value;
	} else {
		return is_array ( $value ) ? array_map ( 'addslashes_deep', $value ) : addslashes ( $value );
	}
}
/**
 * 将对象成员变量或者数组的特殊字符进行转义
 *
 * @access   public
 * @param    mix        $obj      对象或者数组
 * @author   Xuan Yan
 *
 * @return   mix                  对象或者数组
 */
function addslashes_deep_obj($obj) {
	if (is_object ( $obj ) == true) {
		foreach ( $obj as $key => $val ) {
			$obj->$key = addslashes_deep ( $val );
		}
	} else {
		$obj = addslashes_deep ( $obj );
	}
	
	return $obj;
}
/**
 * 递归方式的对变量中的特殊字符去除转义
 *
 * @access  public
 * @param   mix     $value
 *
 * @return  mix
 */
function stripslashes_deep($value) {
	if (empty ( $value )) {
		return $value;
	} else {
		return is_array ( $value ) ? array_map ( 'stripslashes_deep', $value ) : stripslashes ( $value );
	}
}
/**
 *  将一个字串中含有全角的数字字符、字母、空格或'%+-()'字符转换为相应半角字符
 *
 * @access  public
 * @param   string       $str         待转换字串
 *
 * @return  string       $str         处理后字串
 */
function make_semiangle($str) {
	$arr = array ('０' => '0', '１' => '1', '２' => '2', '３' => '3', '４' => '4', '５' => '5', '６' => '6', '７' => '7', '８' => '8', '９' => '9', 'Ａ' => 'A', 'Ｂ' => 'B', 'Ｃ' => 'C', 'Ｄ' => 'D', 'Ｅ' => 'E', 'Ｆ' => 'F', 'Ｇ' => 'G', 'Ｈ' => 'H', 'Ｉ' => 'I', 'Ｊ' => 'J', 'Ｋ' => 'K', 'Ｌ' => 'L', 'Ｍ' => 'M', 'Ｎ' => 'N', 'Ｏ' => 'O', 'Ｐ' => 'P', 'Ｑ' => 'Q', 'Ｒ' => 'R', 'Ｓ' => 'S', 'Ｔ' => 'T', 'Ｕ' => 'U', 'Ｖ' => 'V', 'Ｗ' => 'W', 'Ｘ' => 'X', 'Ｙ' => 'Y', 'Ｚ' => 'Z', 'ａ' => 'a', 'ｂ' => 'b', 'ｃ' => 'c', 'ｄ' => 'd', 'ｅ' => 'e', 'ｆ' => 'f', 'ｇ' => 'g', 'ｈ' => 'h', 'ｉ' => 'i', 'ｊ' => 'j', 'ｋ' => 'k', 'ｌ' => 'l', 'ｍ' => 'm', 'ｎ' => 'n', 'ｏ' => 'o', 'ｐ' => 'p', 'ｑ' => 'q', 'ｒ' => 'r', 'ｓ' => 's', 'ｔ' => 't', 'ｕ' => 'u', 'ｖ' => 'v', 'ｗ' => 'w', 'ｘ' => 'x', 'ｙ' => 'y', 'ｚ' => 'z', '（' => '(', '）' => ')', '〔' => '[', '〕' => ']', '【' => '[', '】' => ']', '〖' => '[', '〗' => ']', '“' => '[', '”' => ']', '‘' => '[', '’' => ']', '｛' => '{', '｝' => '}', '《' => '<', '》' => '>', '％' => '%', '＋' => '+', '—' => '-', '－' => '-', '～' => '-', '：' => ':', '。' => '.', '、' => ',', '，' => '.', '、' => '.', '；' => ',', '？' => '?', '！' => '!', '…' => '-', '‖' => '|', '”' => '"', '’' => '`', '‘' => '`', '｜' => '|', '〃' => '"', '　' => ' ' );
	
	return strtr ( $str, $arr );
}

/**
 *  从POST中取出值
 *
 * @keyname  post键名
 * @ifnull   如值不存在，则赋值为
 *
 * @return  取得后字符串
 */

function get_value_from_post($keyname, $ifnull = false) {
	return get_value_from_array ( $_POST, $keyname, $ifnull );
}



/**
 *  从数组中取出值
 *
 * @arr       数组
 * @key         post键名
 * @default   如值不存在，则赋值为
 *
 * @return  取得后字符串
 */

function get_value_from_array($arr, $key, $default = false) {
	if (! array_key_exists ( $key, $arr )) {
		return $default;
	}
	if (! $arr [$key]) {
		return $default;
	}
	if (is_array ( $arr [$key] )) {
		return $arr [$key];
	} else {
		return trim ( $arr [$key] );
	}

}

/*计算分页参数
$_tc  总条数
$_ps  每页记录数
$_cp  当前页码

返回:

$t['tp'] 总页数
$t['si'] 当前页面记录起始索引
$t['ei'] 当前页面记录结束索引

*/
function pagecalc($_tc, $_ps, $_cp) {
	$tp = $_tc / $_ps;
	if ($_tc % $_ps != 0) {
		$tp = intval ( $tp ) + 1;
	}
	
	if ($_tc < $_ps)
		$tp = 1;
	if ($_cp > $tp)
		$_cp = $tp;
	
	$t ['tp'] = $tp;
	
	$si = ($_cp - 1) * $_ps;
	if ($_cp >= $tp)
		$ei = $_tc + 1;
	else
		$ei = ($_ps * $_cp) - 1;
	
	$t ['si'] = $si;
	$t ['ei'] = $ei;
	$t ['cp'] = $_cp;
	
	if ($_cp > 1) {
		$t ['up'] = $_cp - 1;
	}
	
	if ($_cp < $tp) {
		$t ['np'] = $_cp + 1;
	}
	
	return $t;
}

//去除ubb标签内容
function strip_ubb($str) {
	$str = str_replace ( '[', '<', $str );
	$str = str_replace ( ']', '>', $str );
	$str = strip_tags ( $str );
	return $str;
}

//获得文件扩展名
function get_filetype($path) {
	$pos = strrpos ( $path, '.' );
	if ($pos !== false) {
		return substr ( $path, $pos );
	} else {
		return '';
	}
}

function instr($str, $needle) {
	if (strstr ( $str, $needle )) {
		return true;
	} else {
		return false;
	}
}

//通过起始、结束的标记字符串，获得中间的内容
function get_str_by_tag($str, $start, $end) {
	if (! instr ( $str, $start )) {
		return false;
	}
	if (! instr ( $str, $end )) {
		return false;
	}
	$s = strpos ( $str, $start ) + strlen ( $start );
	$s1 = substr ( $str, $s, strlen ( $str ) );
	$e = strpos ( $s1, $end ) - strlen ( $s1 );
	$s2 = substr ( $s1, 0, $e );
	return $s2;
}

//设置COOKIES
function set_cookie($key, $value, $expire = false) {
	global $timestamp;
	if (! $expire) {
		$expire = $timestamp + 60 * 60;
		//$expire=null;
	}
	setcookie ( COOKIE_PREFIX . $key, $value, $expire, COOKIE_PATH, COOKIE_DOMAIN );
	return $value;
}

//得到COOKIES
function get_cookie($key) {
	$s = get_value_from_array ( $_COOKIE, COOKIE_PREFIX . $key );
	
	return $s;
}

//是否为手机号码
function is_mobile($str) {
	if (strlen ( $str ) != 11) {
		return false;
	}
	if (! is_numeric ( $str )) {
		return false;
	}
	$prefix = substr ( $str, 0, 2 );
	if ($prefix == '10' || $prefix == '11' || $prefix == '12') {
		return false;
	}
	return true;
}

function convertip_tiny($ip) {

    static $fp = NULL, $offset = array(), $index = NULL;
    $ipdatafile=ROOT_PATH.'bbs/ipdata/tinyipdata.dat';

    $ipdot = explode('.', $ip);
    $ip    = pack('N', ip2long($ip));

    $ipdot[0] = (int)$ipdot[0];
    $ipdot[1] = (int)$ipdot[1];

    if($fp === NULL && $fp = @fopen($ipdatafile, 'rb')) {
        $offset = unpack('Nlen', fread($fp, 4));
        $index  = fread($fp, $offset['len'] - 4);
    } elseif($fp == FALSE) {
        return  '- Invalid IP data file';
    }

    $length = $offset['len'] - 1028;
    $start  = unpack('Vlen', $index[$ipdot[0] * 4] . $index[$ipdot[0] * 4 + 1] . $index[$ipdot[0] * 4 + 2] . $index[$ipdot[0] * 4 + 3]);

    for ($start = $start['len'] * 8 + 1024; $start < $length; $start += 8) {

        if ($index{$start} . $index{$start + 1} . $index{$start + 2} . $index{$start + 3} >= $ip) {
            $index_offset = unpack('Vlen', $index{$start + 4} . $index{$start + 5} . $index{$start + 6} . "\x0");
            $index_length = unpack('Clen', $index{$start + 7});
            break;
        }
    }

    fseek($fp, $offset['len'] + $index_offset['len'] - 1024);
    if($index_length['len']) {
        return '- '.fread($fp, $index_length['len']);
    } else {
        return '- Unknown';
    }

}

?>