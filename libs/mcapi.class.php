<?php
/**
 * 发起者为MC的API封装
 * 
 * @author sujiahui<sujiahui@joome.net>
 * @copyright Joome Inc.
 *
 */
class mcapi {
	private $PK;
	private $api;
	private $timestamp;
	
	public function __construct($api) {
		$this->api = $api;
		$this->timestamp=get_utc_timestamp();
	}

	
    /**
     * AS 通知 CL 鉴权通过,并开放权限
     * 
     * @param string $TOKEN 认证令牌
     * @return int $ret 返回值代码
     */

	public function TKOK($TOKEN){
		$arr=array(
				'TOKEN'=>$TOKEN,
		);
		$_d=$this->api_request($arr);
		$request_url=$this->api."?_d={$_d}CMD=MCRQ";
		$ret=file_get_contents($request_url);
		return $ret;
	}
	/**
	 * 将退出登录请求转发给CL处理并执行
	 * 
	 * @param string $TSE
	 * @param string $URL
	 * @param string $TOK
	 * @param string $KEY
	 * @return NULL 重定向至退出登录后的落地URL
	 */
	public function QURQ($TSE,$URL,$TOK,$KEY){
		$arr=array(
			'TSE'=>$TSE,		
			'URL'=>$URL,
			'TOK'=>$TOK,
		);
		$_d=$this->api_request_encode($arr,$KEY);
		$request_url=$this->as_api."?_d={$_d}&CMD=QURQ&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret,$KEY);
		return $arr;
	}
	
	/**
	 *
	 * 对API请求参数数组进行编码
	 * @param array $arr 请求参数数组
	 *
	 * @return string 编码后的_d;
	 */
	private function api_request($arr){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$json;
		return $_d;
	}
	/**
	 * 
	 * 对API请求参数数组进行加密
	 * @param array $arr 请求参数数组
	 * 
	 * @return string 加密后的_d;
	 */
	private function api_request_encode($arr,$KEY){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$this->api_encrypt($json,$KEY);
		$_d=urlencode($_d);		
		return $_d;
	}
	

	
	/**
	 * 
	 * 对API返回结果解密
	 * @param string $_d 需要解密的字符串
	 * @param string $KEY 密匙
	 * 
	 * @return array 解密后的数组;
	 */
	private function api_response_decode($_d,$KEY){
		if(instr($_d, '%')){
			$_d=urldecode($_d);
		}
 		$json=$this->api_decrypt($_d,$KEY);
 		$arr=json_decode($json,true);
		return $arr;
	}
	
	
	private function api_encrypt($encrypt,$KEY) {
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, $KEY, $encrypt, MCRYPT_MODE_ECB, $iv );
		$encode = base64_encode ( $passcrypt );
		return $encode;
	}
	
	private function api_decrypt($decrypt,$KEY) {
		$decoded = base64_decode ( $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $KEY, $decoded, MCRYPT_MODE_ECB, $iv );
		return trim ( $decrypted );
	}

}