<?php
/**
 * 发起者为CL的API封装
 * 
 * @author sujiahui<sujiahui@joome.net>
 * @copyright Joome Inc.
 *
 */
class clapi {
	private $CK;
	private $as_api;
	private $CLID;
	private $timestamp;
	private $SK;
	
	public function __construct($CLID, $CK, $as_api) {
		$this->CK = $CK;
		$this->CLID = $CLID;
		$this->as_api = $as_api;
		$this->timestamp=get_utc_timestamp();
	}

	/**
	 * CL 通过该 API 向 AS 汇报当前运行状态、获取各种维护操作指令。心跳间隔一般为 10 分钟。
	 * 
	 * @param  $FM        剩余内存
	 * @param  $OL        在线用户数
	 * @param  $VER       当前固件版本
	 * @param  $HW        当前硬件类别
	 * @param  $ST        当前软件类别
	 * @param  $CL_API    CLAPI地址
	 * @param  $WAN_RX    WAN端口接收流量
	 * @param  $WAN_TX    WAN端口发送流量
	 * @param  $LAN_RX    LAN端口接收流量
	 * @param  $LAN_TX    LAN端口发送流量
	 * @param  $LOAD      CPU负荷
	 * @param  $WDS       WDS状态
	 * @param  $MACWL     Mac地址白名单
	 * @param  $SPCN      国内网站到达速度
	 * @param  $SPPS      抵达PS_API的速度
	 * @param  $SPOT      海外网站到达速度
	 * @param  $SPBL      被GFW网站到达速度
	 * @param  $OPN       是否为开机首次心跳
	 * 
	 * @return array
	 */
	public function PING($FM,$OL,$VER,$HW,$ST,$CL_API,$WAN_RX,$WAN_TX,$LAN_RX,$LAN_TX,$LOAD,$WDS,$MACWL,$SPCN,$SPPS,$SPOT,$SPBL,$OPN){
		$arr=array(
			    'CLID'=>$this->CLID,		
				'FM'=>$FM,
				'OL'=>$OL,
				'VER'=>$VER,
				'HW'=>$HW,
				'ST'=>$ST,
				'CL_API'=>$CL_API,
				'WAN_RX'=>$WAN_RX,
				'WAN_TX'=>$WAN_TX,
				'LAN_RX'=>$LAN_RX,
				'LAN_TX'=>$LAN_TX,
				'LOAD'=>$LOAD,
				'WDS'=>$WDS,
				'MACWL'=>$MACWL,
				'SPCN'=>$SPCN,
				'SPPS'=>$SPPS,
				'SPOT'=>$SPOT,
				'SPBL'=>$SPBL,
				'OPN'=>$OPN,				
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=PING&CLID=".$this->CLID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	/**
	 * CL向AS发起用户鉴权请求
	 * 
	 * @param string $MAC     MC的mac地址
	 * @param string $CL_API  CL_API
	 * @param string $REF     用户发起请求的原始URL
	 * 
	 * @return null 重定向至 AS 的鉴权页面,或由 AS 继续重定向到 PS 的鉴权页面
	 */
	public function MCRQ($MAC,$CL_API,$REF){
		$arr=array(
				'CLID'=>$this->CLID,
				'MAC'=>$MAC,
				'CL_API'=>$CL_API,
				'REF'=>$REF,
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=MCRQ&CLID=".$this->CLID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	/**
	 * CL 通过该 API 向 AS 获取在用户认证前可以访问的 IP 白名单
	 * 
	 * @return array 一个 json 数组,内含所有白名单 IP
	 */
	public function ALQU(){
		$arr=array(
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=ALQU&CLID=".$this->CLID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	/**
	 * 请求AS对通过16号命令下行的vpn证书文件进行验证，方法是将ca.crt做md5后，使用本接口传递md5，AS返回该md5是否合法。
	 * 
	 * @param string $MD5
	 * @return int $arr CODE
	 */
	public function CAAU($MD5){
		$arr=array(
				'CLID'=>$this->CLID,
				'MD5'=>$MD5,
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=CAAU&CLID=".$this->CLID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	/**
	 * 如果通过PING命令返回的指令被执行失败，则该接口会被调用，以通知AS对命令进行回滚，准备下次重试。
	 * 
	 * @param string $CMDID 命令序列号
	 * @return int $arr CODE
	 */
	public function CMFL($CMDID){
		$arr=array(
				'CLID'=>$this->CLID,
				'CMDID'=>$CMDID,
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=CMFL&CLID=".$this->CLID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	/**
	 * 
	 * 对API请求参数数组进行加密
	 * @param array $arr 请求参数数组
	 * 
	 * @return string 加密后的_d;
	 */
	private function api_request_encode($arr){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$this->api_encrypt($json);
		$_d=urlencode($_d);		
		return $_d;
	}
	

	
	/**
	 * 
	 * 对API返回结果解密
	 * @param string $_d 需要解密的字符串
	 * 
	 * @return array 解密后的数组;
	 */
	private function api_response_decode($_d){
		if(instr($_d, '%')){
			$_d=urldecode($_d);
		}
 		$json=$this->api_decrypt($_d);
 		$arr=json_decode($json,true);
		return $arr;
	}
	
	
	private function api_encrypt($encrypt) {
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, $this->CK, $encrypt, MCRYPT_MODE_ECB, $iv );
		$encode = base64_encode ( $passcrypt );
		return $encode;
	}
	
	private function api_decrypt($decrypt) {
		$decoded = base64_decode ( $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $this->CK, $decoded, MCRYPT_MODE_ECB, $iv );
		return trim ( $decrypted );
	}

}