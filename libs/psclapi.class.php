<?php
/**
 * psclAPI综合调用类
 * @author sujiahui<sujiahui@joome.net>
 *
 */
class psclapi {
	private $KEY;//PK,CK
	private $api;
	private $ID;//PID ,CLID
	private $timestamp;
	
	public function __construct($ID, $KEY, $api) {
		$this->KEY = $KEY;
		$this->ID = $ID;
		$this->api = $api;
		$this->timestamp=get_utc_timestamp();
	}


	
	/**
	 * AS 将鉴权请求转发给 PS 处理
	 * 
	 * @param string $CLID 
	 * @param string $TSE 登入Session编号
	 * @param string $REF 用户发起请求的原始URL
	 * @param string $MCID 表示用户设备的唯一编号
	 * @return NULL 重定向至 PS 的鉴权页面,由 PS 处理鉴权并返回结果
	 */
	public function AUFW($_d){
		$arr=$this->api_response_decode($_d);
		return $arr;	
	}
	
	/**
	 * PS 将鉴权通过结果通知 AS
	 *
	 * @param  $TSE 登入Session编号
	 * @param  $TL 允许MC上网时长
	 * @param  $URL 鉴权通过后，跳转到哪个URL
	 * @param  $QOSLEV 指定该用户的QOS等级 取值为1～5，数字越小，带宽优先级越高
	 * @param  $TOK 自定义的内容,将带此参数跳到目标 URL,以供代理做 SSO 标记使用
	 * @return null 重定向至 AS,由 AS 重定向至 CL_API
	 * @author sujiahui<sujiahui@joome.net>
	 */
	public function AURT($ret_arr){

		$_d=$this->api_request_encode($ret_arr);
		$url=$this->api."?_d={$_d}&CMD=AURT&PID=".$this->ID;
		header ( "Location:" . $url );
		
		return;
	}
	public function open($code){
		$arr=$this->api_response_decode($code);
		return $arr;
		
	}

	/**
	 *  从数组中取出值
	 *
	 * @arr       数组
	 * @key         post键名
	 * @default   如值不存在，则赋值为
	 *
	 * @return  取得后字符串
	 */
	
	private function get_value_from_array($arr, $key, $default = false) {
		if (! array_key_exists ( $key, $arr )) {
			return $default;
		}
		if (! $arr [$key]) {
			return $default;
		}
		if (is_array ( $arr [$key] )) {
			return $arr [$key];
		} else {
			return trim ( $arr [$key] );
		}
	
	}
	
	/**
	 * 
	 * 对API请求参数数组进行加密
	 * @param array $arr 请求参数数组
	 * 
	 * @return string 加密后的_d;
	 */
	private function api_request_encode($arr){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$this->api_encrypt($json);
		$_d=urlencode($_d);		
		return $_d;
	}
	

	
	/**
	 * 
	 * 对API返回结果解密
	 * @param string $_d 需要解密的字符串
	 * 
	 * @return array 解密后的数组;
	 */
	private function api_response_decode($_d){
		if(instr($_d, '%')){
			$_d=urldecode($_d);
		}
 		$json=$this->api_decrypt($_d);
 		$arr=json_decode($json,true);
		return $arr;
	}
	
	
	private function api_encrypt($encrypt) {
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, $this->KEY, $encrypt, MCRYPT_MODE_ECB, $iv );
		$encode = base64_encode ( $passcrypt );
		return $encode;
	}
	
	private function api_decrypt($decrypt) {
		$decoded = base64_decode ( $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $this->KEY, $decoded, MCRYPT_MODE_ECB, $iv );
		return trim ( $decrypted );
	}

}
