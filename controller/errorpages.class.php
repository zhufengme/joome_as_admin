<?php

class errorpages extends controller {
	public function __construct(){
		parent::__construct();
	}
	public function default_action(){

	}
	
	public function nf(){
		$this->set_view("errorpages");
		$this->view_display('404.tpl');
		return;
	}
	
	public function sf(){
		$this->set_view("errorpages");
		$this->view_display('500.tpl');
		return;
	}
	
}	