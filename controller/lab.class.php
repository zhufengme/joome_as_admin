<?php
/**
 * lab.class.php    LAB控制类
 * @Author			sujiahui<sujiahui@joome.net>
 * @since:			2012-05-15
 * @CopyRight		Joome Inc.	
 */
class lab extends controller {
	public function __construct(){
		parent::__construct();

	}
	public function default_action(){
		$this->index();
	}
	public function index(){
		$this->set_view("lab");
		$this->view_display('index.tpl');
		return;
	}

	public function what_is_joome(){
		$this->set_view("lab");
		$this->view_display('what-is-joome.tpl');
		return;
	}
	public function to_be_a_joomer(){
		$this->set_view("lab");
		$this->view_display('to-be-joomer.tpl');
		return;
	}
	public function open_protocols(){
		$this->set_view("lab");
		$this->view_display('open-protocols.tpl');
		return;
	}
	public function faq(){
		$this->set_view("lab");
		$this->view_display('faq.tpl');
		return;
	}
	public function contact(){
		$this->set_view("lab");
		$this->view_display('contact.tpl');
		return;
	}
	public function privacy(){
		$this->set_view("lab");
		$this->view_display('privacy.tpl');
		return;
	}
	
	
	
}