<table align="center" border="1" width=900>
	<caption><h1>{$title}</h1></caption>

	<tr>
		
		<th>节点编号(CLID)</th>
		<th>最近一次开机时间(OPN)</th>
		<th>最近一次心跳时间(LAST)</th>
		<th>当前固件版本(VER)</th>
		<th>剩余内存(FM)</th>
		<th>硬件类别(HW)</th>
		<th>软件类别(ST)</th>
		<th>在线人数(OL)</th>
		<th>WAN端口接收流量(WAN_RX)</th>
		<th>WAN端口发送流量(WAN_TX)</th>
		<th>CPU负荷(CPULOAD)</th>
		<th>WDS状态(WDS)</th>
<!-- 		<th>国内网站到达速度(SPCN)</th>
		<th>PS_API到达速度(SPPS)</th>
		<th>被GFW网站到达速度(SPBL)</th>
		<th>海外网站到达速度(SPOT)</th>
		<th>到达AS_API的速度(SPAS)</th>
		<th>mac地址白名单(MACWL)</th>
		<th>CL_API地址(CL_API)</th>
		<th>CL的接入IP地址(A_IP)</th>
		<th>CL的WAN口IP(W_IP)</th> -->
	</tr>

	{section loop=$datas name=ls}
		<tr>
			<td>{$datas[ls].CLID}</td>
			<td>{$datas[ls].OPN}</td>
			<td>{$datas[ls].LAST}</td>
			<td>{$datas[ls].VER}</td>
			<td>{$datas[ls].FM}</td>
			<td>{$datas[ls].HW}</td>
			<td>{$datas[ls].ST}</td>
			<td>{$datas[ls].OL}</td>
			<td>{$datas[ls].WAN_RX}</td>
			<td>{$datas[ls].WAN_TX}</td>
			<td>{$datas[ls].CPULOAD}</td>
			<td>{$datas[ls].WDS}</td>
<!-- 			<td>{$datas[ls].SPCN}</td>
			<td>{$datas[ls].SPPS}</td>
			<td>{$datas[ls].SPBL}</td>
			<td>{$datas[ls].SPOT}</td>
			<td>{$datas[ls].SPAS}</td>
			<td>{$datas[ls].MACWL}</td>
			<td>{$datas[ls].CL_API}</td>
			<td>{$datas[ls].A_IP}</td>
			<td>{$datas[ls].W_IP}</td>
 -->		</tr>
	{sectionelse}
		<tr>
			<td colspan="7">没有数据</td>
		</tr>
	{/section}
</table>