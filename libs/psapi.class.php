<?php
class psapi {
	private $PK;
	private $as_api;
	private $PID;
	private $timestamp;
	
	public function __construct($PID, $PK, $as_api) {
		$this->PK = $PK;
		$this->PID = $PID;
		$this->as_api = $as_api;
		$this->timestamp=get_utc_timestamp();
	}
	/**
	 * 
	 * PS 通过该 API 向 AS 查询AS的运行状况。
	 * 
	 * @return $arr 返回参数数组
	 */
	public function ASQU(){
		$arr=array(
			'PID'=>$this->PID,			
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=ASQU&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	/**
	 * 
	 * PS 通过该 API 向 CL下发各种操作指令，实际执行时间延迟20分钟，周期执行一个指令，如遇CL关机，则在开机后执行。
	 * 
	 *@param $CLID CL的唯一编号
	 *@param $CODE 操作指令代码
	 *@return $arr 返回参数数组
     * @author sujiahui<sujiahui@joome.net>
	 */
	public function CLCM($CLID, $CODE){
		$arr=array(
				'PID'=>$this->PID,
				'CLID'=>$CLID,
				'CODE'=>$CODE,
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=CLCM&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	/**
	 * 
	 * PS 通过该 API 向 AS 查询特定 CL 的状态信息。
	 * 
	 * @param  $CLID CL的唯一编号
	 * @return $arr 返回参数数组
	 * @author sujiahui<sujiahui@joome.net>
	 */
	
	public function CLQU($CLID){
		$arr=array(
				'PID'=>$this->PID,
				'CLID'=>$CLID,
				
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=CLQU&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	/**
	 * PS向AS发起用户鉴权请求，并获得认证令牌，用认证令牌可以实现在app或者应用程序中的鉴权。
	 * 
	 * @param  $CLID  
	 * @param  $MAC
	 * @param  $TL
	 * @param  $QOSLEV
	 * @return $arr 返回参数数组
	 * @author sujiahui<sujiahui@joome.net>
	 */
	public function TKRQ($CLID,$MAC,$TL,$QOSLEV){
		$arr=array(
				'CLID'=>$CLID,
				'MAC'=>$MAC,
				'TL'=>$TL,
				'QOSLEV'=>$QOSLEV,
	
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=TKRQ&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	/**
	 * PS 将鉴权通过结果通知 AS
	 * 
	 * @param  $TSE 登入Session编号
	 * @param  $TL 允许MC上网时长
	 * @param  $URL 鉴权通过后，跳转到哪个URL
	 * @param  $QOSLEV 指定该用户的QOS等级 取值为1～5，数字越小，带宽优先级越高
	 * @param  $TOK 自定义的内容,将带此参数跳到目标 URL,以供代理做 SSO 标记使用 
	 * @return 重定向至 AS,由 AS 重定向至 CL_API 
	 * @author sujiahui<sujiahui@joome.net>
	 */
	public function AURT($TSE,$TL,$URL,$QOSLEV,$TOK){
		$arr=array(
				'TSE'=>$TSE,
				'TL'=>$TL,
				'URL'=>$URL,
				'QOSLEV'=>$QOSLEV,
				'TOK'=>$TOK,
	
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=AURT&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	/**
	 * PS 通过该 API 向 AS 查询特定 TSE 的详细信息。
	 * 
	 * @param  $TSE 登入Session编号
	 * @author sujiahui<sujiahui@joome.net>
	 */
	public function TEQU($TSE){
		$arr=array(
				'PID'=>$this->PID,
				'TSE'=>$TSE,
		);
		$_d=$this->api_request_encode($arr);
		$request_url=$this->as_api."?_d={$_d}&CMD=AURT&PID=".$this->PID;
		$ret=file_get_contents($request_url);
		$arr=$this->api_response_decode($ret);
		return $arr;
	}
	
	
	/**
	 * 
	 * 对API请求参数数组进行加密
	 * @param array $arr 请求参数数组
	 * 
	 * @return string 加密后的_d;
	 */
	private function api_request_encode($arr){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$this->api_encrypt($json);
		$_d=urlencode($_d);		
		return $_d;
	}
	

	
	/**
	 * 
	 * 对API返回结果解密
	 * @param string $_d 需要解密的字符串
	 * 
	 * @return array 解密后的数组;
	 */
	private function api_response_decode($_d){
		if(instr($_d, '%')){
			$_d=urldecode($_d);
		}
 		$json=$this->api_decrypt($_d);
 		$arr=json_decode($json,true);
		return $arr;
	}
	
	
	private function api_encrypt($encrypt) {
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, $this->PK, $encrypt, MCRYPT_MODE_ECB, $iv );
		$encode = base64_encode ( $passcrypt );
		return $encode;
	}
	
	private function api_decrypt($decrypt) {
		$decoded = base64_decode ( $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $this->PK, $decoded, MCRYPT_MODE_ECB, $iv );
		return trim ( $decrypted );
	}

}