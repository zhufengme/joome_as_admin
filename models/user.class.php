<?php
/**
 * 
 * 用户类
 * @author zhufengme
 *
 */

class user extends model {
	
	private $UID = null;
	
	public function __construct($UID) {
		parent::__construct ();
		$this->UID = $UID;
	}
	
	public function __get($name) {
		eval ( "return get_{$name}();" );
	}
	
	/**
	 * 获得总在线时间，单位：分钟
	 */
	public function get_online_time() {
		$online_time = $this->db->get_var ( "select online_time from t_users_list where UID={$this->UID}" );
		return $online_time;
	}
	
	/**
	 * 获得本月在线时间，单位：分钟
	 */
	public function get_online_time_month() {
		$month = date ( "m", $this->timestamp );
		$year = date ( "Y", $this->timestamp );
		$start = mktime ( 0, 0, 0, $month, 1, $year );
		$end = mktime ( 0, 0, - 1, $month + 1, 1, $year );
		
		$start_time = helper::localtime_to_utctime ( $start, $this->get_timezone () );
		$end_time = helper::localtime_to_utctime ( $end, $this->get_timezone () );
		$total_time = $this->db->get_var ( "select sum(total_time) from t_users_online_log where online_time>=$start_time and offline_time<=$end_time and offline_time>0" );
		return intval ( $total_time / 60 );
	
	}
	
	/**
	 *
	 *
	 * 记录用户在线时间明细
	 * 
	 * @param string $CLID
	 *        	所在路由器编号
	 * @param int $online_time
	 *        	上线时间，时间戳格式
	 * @param int $offline_time
	 *        	离线时间（可选，如不填写，需要使用set_offline来记录下线时间）
	 *        	
	 */
	public function add_online_log($CLID, $online_time, $offline_time = 0) {
		if ($offline_time > 0 && $offline_time <= $online_time) {
			return false;
		}
		
		if ($offline_time > 0) {
			$total_time = $offline_time - $online_time;
			$this->db->query ( "update t_users_list set online_time=$total_time where UID={$this->UID}" );
		} else {
			$total_time = 0;
		}
		
		$ret = $this->db->query ( "insert into t_users_online_log (UID,online_time,offline_time,total_time,CLID) values ({$this->UID},$online_time,$offline_time,$total_time,'$CLID')" );
		
		if ($ret) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 *
	 *
	 * 设置用户离线时间
	 * 
	 * @param string $CLID
	 *        	所在路由器编号
	 * @param int $offline_time
	 *        	离线时间，时间戳格式
	 */
	public function set_offline_to_log($CLID, $offline_time) {
		$row = $this->db->get_row ( "select olid,online_time from t_users_online_log where offline_time=0 and CLID='$CLID' and UID={$this->UID} and online_time<$offline_time order by online_time limit 1" );
		$total_time = $offline_time - $row->online_time;
		$this->db->query ( "update t_users_list set online_time=$total_time where UID={$this->UID}" );
		$ret = $this->db->query ( "update t_users_online_log set offline_time=$offline_time,total_time=$total_time where olid={$row->olid}" );
		if ($ret) {
			return true;
		} else {
			return false;
		}
	
	}
	
	/**
	 * 获取用户本地时间戳
	 */
	public function get_user_timestamp() {
		$timestamp = helper::get_utc_timestamp ();
		$offset = $this->get_timezone ();
		$offset = $offset * 60 * 60;
		return $timestamp + $offset;
	}
	
	/**
	 * 获得总积分
	 */
	public function get_point() {
		$point = $this->db->get_var ( "select point from t_users_list where UID={$this->UID}" );
		return $point;
	}
	
	/**
	 * 取得用户名
	 */
	public function get_username() {
		$username = $this->db->get_var ( "select username from t_users_list where UID={$this->UID}" );
		return $username;
	}
	
	/**
	 * 取得用户时区
	 */
	public function get_timezone() {
		$timezone = $this->db->get_var ( "select timezone from t_users_list where UID={$this->UID}" );
		return $timezone;
	}
	
	/**
	 *
	 *
	 * 透过AS传来的MCID生成一个虚用户
	 * 
	 * @param string $as_mcid        	
	 *
	 */
	public static function get_user_by_as_mcid($as_mcid) {
		$db = self::get_db_connect ();
		$UID = $db->get_var ( "select UID from t_mcs_list where as_mcid='$as_mcid'" );
		
		if ($UID) {
			return new user ( $UID );
		}
		
		$user_agent = Application::get_user_agent ();
		$user_agent = addslashes ( $user_agent );
		$timestamp = helper::get_utc_timestamp ();
		
		$db->query ( "insert into t_mcs_list (as_mcid,is_bind,user_agent,log_time) values ('$as_mcid',0,'$user_agent',$timestamp)" );
		$MCID = $db->insert_id;
		$username = rand ( 100, 999 ) . $MCID;
		$db->query ( "insert into t_users_list (username) values ('$username')" );
		$UID = $db->insert_id;
		$db->query ( "update t_mcs_list set UID='$UID' where mcid='$MCID'" );
		
		return new user ( $UID );
	
	}
	/**
	 * 用户注册+验证
	 * 
	 * @param unknown_type $username        	
	 * @param unknown_type $password        	
	 * @param unknown_type $email        	
	 * @param unknown_type $phone        	
	 * @param unknown_type $code
	 *        	验证是否成功
	 */
	public function register($username, $password, $email, $phone, $code, $as_mcid) {
		$db = self::get_db_connect ();
		$log_time = $db->get_var ( "select now()" );
		$password = md5 ( $password . $log_time );
		$lang = $this->get_lang ();
		$device = $this->get_device ();
		
		$user_agent = Application::get_user_agent ();
		$user_agent = addslashes ( $user_agent );
		$timestamp = helper::get_utc_timestamp ();
		// 插入mcid
		$db->query ( "insert into t_mcs_list (as_mcid,is_bind,user_agent,log_time) values ('$as_mcid',0,'$user_agent',$timestamp)" );
		$MCID = $db->insert_id;
		// 更新用户表
		$db->query ( "insert into t_users_list (is_active,is_fullinfo,username,password,email,log_time,langs) values (1,0,'$username','$password','$email',now(),'$lang')" );
		// 查询用户UID编号
		$UID = $db->get_var ( "select UID from t_users_list where username='$username'" );
		// 更新 t_mcs_list 表 Uid字段
		$db->query ( "update t_mcs_list set UID=$UID where mcid=$MCID" );
		// 插入$phone用户实名认证信息表
		$db->query ( "insert into t_users_verify (UID,mphone,mphone_verd) values ($UID,$phone,1)" );
		// 插入用户积分变动表
		$db->query ( "insert into t_users_point_log (UID,reason,log_time) values ('$UID',1001,now())" );
		return new user ( $UID );
	}
	/**
	 * 用户注册
	 * 
	 * @param unknown_type $username        	
	 * @param unknown_type $password        	
	 * @param unknown_type $email        	
	 */
	public function reg($username, $password, $email, $code, $as_mcid) {
		$db = self::get_db_connect ();
		$log_time = $db->get_var ( "select UNIX_TIMESTAMP(now())" );
		$password = md5 ( $password . $log_time );
		$lang = $this->get_lang ();
		
		$user_agent = Application::get_user_agent ();
		$user_agent = addslashes ( $user_agent );
		$timestamp = helper::get_utc_timestamp ();
		// 先删除之前建立的虚拟用户
		$UID = $db->get_var ( "select UID from t_mcs_list  where as_mcid='$as_mcid'" );
		$db->query ( "delete from t_mcs_list where as_mcid='$as_mcid'" );
		$db->query ( "delete from t_users_list where UID='$UID'" );
		
		// 插入mcid
		$db->query ( "insert into t_mcs_list (as_mcid,is_bind,user_agent,log_time) values ('$as_mcid',0,'$user_agent',$timestamp)" );
		$MCID = $db->insert_id;
		
		// 更新用户表
		$db->query ( "insert into t_users_list (is_active,is_fullinfo,username,password,email,log_time,langs) values (1,0,'$username','$password','$email','$log_time','$lang')" );
		$UID = $db->insert_id;
		
		// 更新 t_mcs_list 表 Uid字段
		$db->query ( "update t_mcs_list set UID='$UID' where mcid='$MCID'" );
		
		// 插入用户积分变动表 注册奖励 代码为1001
		$db->query ( "insert into t_users_point_log (UID,reason,log_time) values ('$UID',1001,'$log_time')" );
		return new user ( $UID );
	
	}
	/**
	 * 用户登录
	 * 
	 * @param unknown_type $username        	
	 * @param unknown_type $password        	
	 * @return object user
	 * @return false 登录失败
	 */
	public static function login($username, $password, $as_mcid) {
		$db = self::get_db_connect ();
		// 判断账号类型 这里还是有问题 主要是helper
//  		if (helper::is_email ( $username )) {
// 			$log_time = $db->get_var ( "select log_time from t_users_list where email='$username'" );
// 			$password_test = md5 ( $password . $log_time );
// 			$password = $db->get_var ( "select password from t_users_list where email='$username'" );
		
// 		} elseif (helper::is_mobile ( $username )) {
		
// 		} else { 
			$log_time = $db->get_var ( "select log_time from t_users_list where username='$username'" );
			$password_test = md5 ( $password . $log_time );
			$password = $db->get_var ( "select password from t_users_list where username='$username'" );
	//	}
		// 验证密码是否正确
		if ($password == $password_test) {

			 //写添加 新的mac地址 as_mcid 用于记录
			 //但是之前的设备不能登录
			 //需要写一个方法用检测用户拥有的所有mac地址
		 	$UID = $db->get_var ( "select UID from t_users_list where username='$username'" );
			return new user ( $UID ); 
	
		} 
		else {
			return 0;
		}
	
	}
	/**
	 * 统计本网用户数
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_sum_users($CLID) {
		$usertime = $this->get_user_timestamp ();
		$usertime = helper::localtime_to_utctime ( $usertime, $this->get_timezone () );
		$user_num = $this->db->get_var ( "select count(UID) from t_users_online_log where CLID ='$CLID' and online_time < '$usertime' and offline_time > '$usertime'" );
		return $user_num;
	}
	/**
	 * 得到分享者用户等级，等级规则的实现
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_user_level($CLID) {
		// $level = 0;
		$usertime = $this->get_user_timestamp ();
		$usertime = helper::localtime_to_utctime ( $usertime, $this->get_timezone () );
		$clid_total_time = $this->db->get_var ( "select DATE_FORMAT(FROM_UNIXTIME(online_time,  '%Y%m%d' ),'%Y-%m') as month,sum(total_time) as total_time from t_users_online_log where DATE_FORMAT(FROM_UNIXTIME(online_time,  '%Y%m%d' ),'%Y')=2012 and  month(FROM_UNIXTIME(online_time,  '%Y%m%d' ))=month(now()) and CLID = {$CLID}  group by month order by month " );
		if (($clid_total_time->total_time < 120 * 60) && ($clid_total_time->total_time >= 0)) {
			$level = 0;
			return $level;
		} elseif (($clid_total_time->total_time >= 120 * 60) && ($clid_total_time->total_time < 300 * 60)) {
			$level = 1;
			return $level;
		} elseif (($clid_total_time->total_time >= 300 * 60) && ($clid_total_time->total_time > 800 * 60)) {
			$level = 2;
			return $level;
		} elseif (($clid_total_time->total_time >= 800 * 60) && ($clid_total_time->total_time > 2000 * 60)) {
			$level = 3;
			return $level;
		} elseif (($clid_total_time->total_time >= 2000 * 60) && ($clid_total_time->total_time > 5000 * 60)) {
			$level = 4;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 5000 * 60) && ($clid_total_time->total_time > 12000 * 60)) {
			$level = 5;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 12000 * 60) && ($clid_total_time->total_time > 30000 * 60)) {
			$level = 6;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 30000 * 60) && ($clid_total_time->total_time > 80000 * 60)) {
			$level = 7;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 80000 * 60) && ($clid_total_time->total_time > 250000 * 60)) {
			$level = 8;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 250000 * 60) && ($clid_total_time->total_time > 1500000 * 60)) {
			$level = 9;
			return $level;
		
		} elseif (($clid_total_time->total_time >= 1500000 * 60) && ($clid_total_time->total_time > 9999999999 * 60)) {
			$level = 10;
			return $level;
		}
	}
	/**
	 * 得到 分享者用户的特权
	 * 
	 * @param unknown_type $level
	 *        	等级
	 * @param unknown_type $TL
	 *        	时长
	 * @return array $power_arr 返回一个数组包含 （时长，带宽等级）
	 */
	public function get_user_power($level, $TL = 0) {
		if ($level == 0) {
			$TL = $TL + 0;
			$QOSLEV = 5;
		} elseif ($level == 1) {
			$TL = $TL + 3600 * 60;
			$QOSLEV = 5;
		} elseif ($level == 2) {
			$TL = $TL + 5400 * 60;
			$QOSLEV = 5;
		} elseif ($level == 3) {
			$TL = $TL + 120 * 60 * 60;
			$QOSLEV = 4;
		} elseif ($level == 4) {
			$TL = $TL * 2;
			$QOSLEV = 4;
		} elseif ($level == 5) {
			$TL = $TL * 2.5;
			$QOSLEV = 3;
		} elseif ($level == 6) {
			$TL = $TL * 3;
			$QOSLEV = 3;
		} elseif ($level == 7) {
			$TL = $TL * 3.5;
			$QOSLEV = 3;
		} elseif ($level == 8) {
			$TL = 99999999999999;
			$QOSLEV = 2;
		} elseif ($level == 9) {
			$TL = 99999999999999;
			$QOSLEV = 1;
		} elseif ($level == 10) {
			$TL = 99999999999999;
			$QOSLEV = 1;
		}
		
		$power_arr = Array (
				'TL' => $TL,
				'QOSLEV' => $QOSLEV 
		);
		return $power_arr;
	}
	/**
	 * 判断用户名是否唯一
	 * 
	 * @param string $username
	 *        	用户名
	 * @param int $UID_num
	 *        	得到用户名注册
	 * @return int $UID_num 输出给前端JSON字符
	 */
	public function user_name_check($username) {
		$db = self::get_db_connect ();
		$UID_num = $db->get_var ( "select count(*) from t_users_list where username='$username'" );
		// 当$UID_num==0时用户名可以注册
		if ($UID_num == 0) {
			//echo json_encode ( $UID_num );
			//return;
			return true;
		} else {
			//echo json_encode ( $UID_num );
			return false;
		}
	}


	//test
	public function lister() {

		$db = self::get_db_connect();
		$array = $db->get_results("select * from nodes_status limit 0,10", ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}
	}

	//test2
	public function checkadmin($acc, $pw){
		$db = self::get_db_connect();
		$sql = "select * from proxy_list where proxy_name = '".$acc."' and password = '".md5($pw)."'";
		$array = $db->get_results($sql, ARRAY_A);
		if (count($array) == 1) {
			return $array;
		} else {
			return false;
		}

	}

	//test3
	public function get_proxy_list(){
		$db = self::get_db_connect();
		$array = $db->get_results("SELECT pid,proxy_name FROM proxy_list ORDER BY pid ASC", ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}		
	}	

	//test4
	public function control_insert($arr){
		$db = self::get_db_connect();
		$sql = "insert into nodes_cmd_queue (CLID,CODE,CMD,logtime) values ('".$arr['CLID']."', '".$arr['CODE']."', '".$arr['CMD']."', '".$arr['logtime']."')";
		$ret = $db->query ($sql);
		
		if ($ret) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * 判断邮箱是否唯一
	 * 
	 * @param string $email
	 *        	邮箱
	 * @param int $email_num
	 *        	得到邮箱注册
	 * @return int $email_num 输出给前端JSON字符
	 */
	public function user_email_check($email) {
		$db = self::get_db_connect ();
		$email_num = $db->get_var ( "select count(*) from t_users_list where email='$email'" );
		// 当$UID_num==0时用户名可以注册
		if ($email_num == 0) {
			//echo json_encode ( $email_num );
			return true;
		} else {
			//echo json_encode ( $email_num );
			return false;
		}
	}
	/**
	 * 通过MCID判断用户是否是注册用户
	 * 
	 * @param unknown_type $as_mcid        	
	 * @return boolean $is_active
	 */
	public static function get_uid_is_active($as_mcid) {
		$db = self::get_db_connect ();
		$UID = $db->get_var ( "select UID from t_mcs_list where as_mcid='$as_mcid'" );
		$is_active = $db->get_var ( "select is_active from t_users_list where UID='$UID'" );
		
		return $is_active;
	
	}
	/**
	 * 通过MCID得到UID 用户ID
	 * 
	 * @param unknown_type $as_mcid        	
	 * @return string $UID
	 */
	public static function get_uid_by_mcid($as_mcid) {
		$db = self::get_db_connect ();
		$UID = $db->get_var ( "select UID from t_mcs_list where as_mcid='$as_mcid'" );
		return $UID;
	}
	
	/**
	 * 得到热点累计分享时长
	 * 
	 * @param unknown_type $CLID        	
	 * @return int 按小时的时长
	 */
	public function get_total_time_by_clid($CLID) {
		
		// $share_total_time_hour = 0;
		$share_total_time = $this->db->get_var ( "select sum(total_time) from t_users_online_log where CLID = '$CLID' " );
		$share_total_time_hour = ($share_total_time - $share_total_time % 3600) / 3600;
		return $share_total_time_hour;
	}
	/**
	 * 得到本地登录次数
	 * 
	 * @param unknown_type $uid
	 *        	用户ID
	 * @param unknown_type $clid
	 *        	CLID
	 * @return int $user_onlinetimes 返回用户本地登录次数
	 */
	public function get_user_online_times($username, $CLID) {
		$UID = $this->db->get_var ( "select UID from t_users_list where username = '$username' " );
		$user_onlinetimes = $this->db->get_var ( "select count(*)  from t_users_online_log where UID= '$UID' and CLID = '$CLID'  " );
		return $user_onlinetimes;
	}
	/**
	 * 得到分享者用户名
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_name($CLID) {
		$db = self::get_db_connect ();
		$owner_name = $db->get_var ( "select username from t_owner_list where CLID = '$CLID' " );
		return $owner_name;
	
	}
	/**
	 * 得到分享者个人签名
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_words($CLID) {
		$db = self::get_db_connect ();
		$owner_words = $db->get_var ( "select words from t_owner_list where CLID = '$CLID' " );
		return $owner_words;
	}
	/**
	 * 得到用户头像链接地址
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_headphoto($CLID) {
		$db = self::get_db_connect ();
		$owner_headphoto = $db->get_var ( "select headphoto from t_owner_list where CLID = '$CLID' " );
		return $owner_headphoto;
	}
	/**
	 * 得到用户社交网络链接
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_link($CLID) {
		$db = self::get_db_connect ();
		$owner_link = $db->get_var ( "select link from t_owner_list where CLID = '$CLID' " );
		return $owner_link;
	}
	
	/**
	 * 更新分享者 级别，分享时间
	 * 
	 * @param unknown_type $username        	
	 * @param unknown_type $level        	
	 * @param unknown_type $total_time        	
	 */
	public function update_owner_list($username, $level, $total_time) {
		$db = self::get_db_connect ();
		$db->query ( "update t_owner_list set level='$level' ,total_time='$total_time' where username='$username'" );
	
	}
	
	/**
	 * 得到分享者等级
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_level($CLID) {
		$db = self::get_db_connect ();
		$owner_level = $db->get_var ( "select level from t_owner_list where CLID = '$CLID' " );
		return $owner_level;
	}
	/**
	 * 得到分享者分享总时间
	 * 
	 * @param unknown_type $CLID        	
	 */
	public function get_owner_total_time($CLID) {
		$db = self::get_db_connect ();
		$owner_total_time = $db->get_var ( "select total_time from t_owner_list where CLID = '$CLID' " );
		return $owner_total_time;
	}
	/**
	 * 验证用户是否超时
	 * 
	 * @param unknown_type $as_mcid        	
	 * @param unknown_type $user_time        	
	 * @return boolean 超时结果 true超时 false没超时
	 */
	public function check_user_timeout($as_mcid, $user_time) {
		$db = self::get_db_connect ();
		$UID = $db->get_var ( "select UID from t_mcs_list where as_mcid='$as_mcid'" );
		$max_off_time = $db->get_var ( "select max(offline_time) from t_users_online_log where UID='$UID'" );
		$num = $db->get_var ( "select count(*) from t_users_online_log where UID='$UID' and ( DATE_FORMAT(FROM_UNIXTIME('$max_off_time'),'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') )  " );
		
		if ($num == 1) {
			$total_time = $db->get_var ( "select total_time from t_users_online_log where UID='$UID' and ( DATE_FORMAT(FROM_UNIXTIME($max_off_time),'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') )  " );
			// 超时
			if ($total_time > $user_time) {
				return true;
			} 			// 没超时
			else {
				return false;
			}
		} elseif ($num == 0) {
			return false;
		}
	
	}

}