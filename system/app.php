<?php
/*
 * 应用入口
 * 
 */

if (__FILE__ == '') {
	die ( 'Fatal error code' );
}
error_reporting ( E_ALL );

define ( 'ROOT_PATH', str_replace ( 'system/app.php', '', str_replace ( '\\', '/', __FILE__ ) ) );
define ( 'LIB_PATH', ROOT_PATH . "libs/" );
define ( 'CONTROLLER_PATH', ROOT_PATH . "controller/" );
define ( 'BASE_PATH', ROOT_PATH . "base/" );
define ( 'MODEL_PATH', ROOT_PATH . "models/" );
define ( 'TPL_PATH', ROOT_PATH . "tpls/" );
define ( 'CACHE_PATH', ROOT_PATH . "cache/" );
define ( 'LOG_PATH', "/var/log/" );

final class Application {
	
	public static function in_page() {
		if (empty ( $_SERVER ['REQUEST_METHOD'] )) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 得到用户UA
	 * 
	 * @return 用户UA字符串
	 * 
	 */
	public static function get_user_agent() {
		if(!Application::in_page()){
			return 'Scripts';
		}
		return $_SERVER['HTTP_USER_AGENT'];
	}
	
	/**
	 * 
	 * 得到当前配置文件数组
	 * 
	 * @return array 配置文件数组
	 * 
	 * 
	 */
	public static function get_config(){
		if(!$CONFIG){
			require ROOT_PATH.'system/config.php';
		}
		return $CONFIG;
	}
	 		
	public static function load_controller($controller_name = false) {
		if (! self::in_page ()) {
			return;
		}
		if (! $controller_name) {
			$controller_name = get_value_from_array ( $_GET, 'c' ,'index');
			if (! $controller_name) {
				header ( "HTTP/1.1 404 controller $controller_name not found" );
				die ();
			}
		}
		if (! file_exists ( CONTROLLER_PATH . $controller_name . ".class.php" )) {
			header ( "HTTP/1.1 404 controller $controller_name not found" );
			die ();
		}
		require_once CONTROLLER_PATH . $controller_name . '.class.php';
		$controller = new $controller_name ();
		$action_name = get_value_from_array ( $_GET, 'act' ,'default_action' );
		
		if ($action_name) {
			if (! method_exists ( $controller, $action_name )) {
				header ( "HTTP/1.1 404 action $action_name not found" );
				die ();
			}
			$str='$controller->'.$action_name.'();';
			eval($str);
		}
		return;
	
	}
	
	public static function load_libs() {
		require_once LIB_PATH . 'common.libs.php';		
		require_once LIB_PATH . 'smarty/Smarty.class.php';
		require_once LIB_PATH . 'ez_sql_core.php';
		require_once LIB_PATH . 'ez_sql_mysql.php';
		require_once BASE_PATH . 'base.class.php';
		require_once BASE_PATH . 'controller.class.php';
		require_once BASE_PATH . 'model.class.php';
		require_once LIB_PATH . 'helper.class.php';		
		require_once LIB_PATH . 'psapi.class.php';
		require_once LIB_PATH . 'testapi.class.php';
		require_once LIB_PATH . 'psclapi.class.php';
		require_once LIB_PATH . 'PageSupport.class.php';		
		
	
	}
	
	public static function enter() {
		define ( 'IS_INIT', true );
		self::load_libs ();
		if (self::in_page ()) {
			session_start ();
			if (! get_magic_quotes_gpc ()) {
				if (! empty ( $_GET )) {
					$_GET = addslashes_deep ( $_GET );
				}
				if (! empty ( $_POST )) {
					$_POST = addslashes_deep ( $_POST );
				}
				
				$_COOKIE = addslashes_deep ( $_COOKIE );
				$_REQUEST = addslashes_deep ( $_REQUEST );
			}
		}
		self::load_controller ();
		return;
	}
}