<?php
/**
 * 
 * Helper 类，用于放置一些常用函数
 * @author zhufengme
 *
 */

class helper {
	
	/**
	 * 
	 * 获取utc时间戳
	 */
	public static function get_utc_timestamp(){
		return time()-date('Z');
	}
	
	/**
	 * 
	 * 将本地时间转换为utc时间戳
	 * 
	 */
	public static function localtime_to_utctime($localtime,$timezone){
		$offset=0-$timezone*60*60;
		return $localtime+$offset;
	}
	/**
	 * 验证输入的邮件地址是否合法
	 *
	 * @access  public
	 * @param   string      $email      需要验证的邮件地址
	 *
	 * @return bool
	 */
	public static function is_email($user_email) {
		$chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
		if (strpos ( $user_email, '@' ) !== false && strpos ( $user_email, '.' ) !== false) {
			if (preg_match ( $chars, $user_email )) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	//是否为手机号码
	public static function is_mobile($str) {
		if (strlen ( $str ) != 11) {
			return false;
		}
		if (! is_numeric ( $str )) {
			return false;
		}
		$prefix = substr ( $str, 0, 2 );
		if ($prefix == '10' || $prefix == '11' || $prefix == '12') {
			return false;
		}
		return true;
	}

}