{*
	{$pageSupport.total_records}<br/>
	{$pageSupport.current_page}<br/>
	{$pageSupport.total_pages}<br/>
	{$pageSupport.first}<br/>
	{$pageSupport.prev}<br/>
	{$pageSupport.next}<br/>
	{$pageSupport.last}<br/>
*}
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$title}</title>
</head>
<body>


<table align="center" border="1" width=900>
	<caption><h1>{$title}</h1></caption>

	<tr>
		
		<th>节点编号(CLID)</th>
		<th>最近一次开机时间(OPN)</th>
		<th>最近一次心跳时间(LAST)</th>
		<th>当前固件版本(VER)</th>
		<th>剩余内存(FM)</th>
		<th>硬件类别(HW)</th>
		<th>软件类别(ST)</th>
		<th>在线人数(OL)</th>
		<th>WAN端口接收流量(WAN_RX)</th>
		<th>WAN端口发送流量(WAN_TX)</th>
		<th>CPU负荷(CPULOAD)</th>
		<th>WDS状态(WDS)</th>
<!-- 		<th>国内网站到达速度(SPCN)</th>
		<th>PS_API到达速度(SPPS)</th>
		<th>被GFW网站到达速度(SPBL)</th>
		<th>海外网站到达速度(SPOT)</th>
		<th>到达AS_API的速度(SPAS)</th>
		<th>mac地址白名单(MACWL)</th>
		<th>CL_API地址(CL_API)</th>
		<th>CL的接入IP地址(A_IP)</th>
		<th>CL的WAN口IP(W_IP)</th> -->
	</tr>

	{section loop=$results name=ls}
		<tr>
			<td>{$results[ls].CLID}</td>
			<td>{$results[ls].OPN}</td>
			<td>{$results[ls].LAST}</td>
			<td>{$results[ls].VER}</td>
			<td>{$results[ls].FM}</td>
			<td>{$results[ls].HW}</td>
			<td>{$results[ls].ST}</td>
			<td>{$results[ls].OL}</td>
			<td>{$results[ls].WAN_RX}</td>
			<td>{$results[ls].WAN_TX}</td>
			<td>{$results[ls].CPULOAD}</td>
			<td>{$results[ls].WDS}</td>
<!-- 			<td>{$results[ls].SPCN}</td>
			<td>{$results[ls].SPPS}</td>
			<td>{$results[ls].SPBL}</td>
			<td>{$results[ls].SPOT}</td>
			<td>{$results[ls].SPAS}</td>
			<td>{$results[ls].MACWL}</td>
			<td>{$results[ls].CL_API}</td>
			<td>{$results[ls].A_IP}</td>
			<td>{$results[ls].W_IP}</td>
 -->		</tr>
	{sectionelse}
		<tr>
			<td colspan="7">没有数据</td>
		</tr>
	{/section}
</table>
    <br>   


<br/>


{if ( $pageSupport.total_records > 0 )}

	<form action="testpage/test" method="get">
	共{$pageSupport.total_records}记录
	第{$pageSupport.current_page}页/共{$pageSupport.total_pages}页
	{if ( $pageSupport.current_page > 1 )}
	   <A href=?current_page={$pageSupport.first}>首页</A>
	   <A href=?current_page={$pageSupport.prev}>上一页</A>
	{/if}
	
	{if ( $pageSupport.current_page < $pageSupport.total_pages )}
	   <A href=?current_page={$pageSupport.next}>下一页</A>
	   <A href=?current_page={$pageSupport.last}>末页</A>
	{/if}
	
	  跳到<input type="text" size="4" name="current_page" value="{$pageSupport.current_page}"/>页
	  <input type="submit" value="GO"/>
	</form>

{/if}


</body>
</html>