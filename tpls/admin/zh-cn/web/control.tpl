<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>操作</title>
</head>
<body>
<a href="{$referer}">点击返回</a>

<table align="center" border="1" width=900>
	<caption><h1>操作</h1></caption>
	<tr>
		<th>节点编号(CLID)</th>
		<th>最近一次开机时间(OPN)</th>
		<th>最近一次心跳时间(LAST)</th>
		<th>当前固件版本(VER)</th>
		<th>剩余内存(FM)</th>
		<th>硬件类别(HW)</th>
		<th>软件类别(ST)</th>
		<th>在线人数(OL)</th>
		<th>WAN端口接收流量(WAN_RX)</th>
		<th>WAN端口发送流量(WAN_TX)</th>
		<th>CPU负荷(CPULOAD)</th>
		<th>WDS状态(WDS)</th>
		<th>代理商ID(pid)</th>
	</tr>
	<tr>
		<td>{$arr.CLID}</td>
		<td>{date('Y-m-d H:i:s', $arr.OPN+3600*8)}</td>
		<td>{date('Y-m-d H:i:s', $arr.LAST+3600*8)}</td>
		<td>{$arr.VER}</td>
		<td>{$arr.FM}</td>
		<td>{$arr.HW}</td>
		<td>{$arr.ST}</td>
		<td>{$arr.OL}</td>
		<td>{$arr.WAN_RX}</td>
		<td>{$arr.WAN_TX}</td>
		<td>{$arr.CPULOAD}</td>
		<td>{$arr.WDS}</td>
		<th>{$arr.pid}</th>
	</tr>
	
</table>

<style>
dl {
  margin-bottom:50px;
}

dl dt {
  background:#5f9be3;
  color:#fff;
  float:left;
  font-weight:bold;
  margin-right:20px;
  padding:5px;
  width:300px;
}

dl dd {
  margin:2px 0;
  padding:5px 0;
}
</style>
<div>
	<h2>命令列表</h2>
	<dl>
		<dt>序号(描述)</dt>
		<dd>操作</dd>
		<dt>3(更改维护密码，CMD为新的维护密码)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="3" type="hidden">
				密码: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		<dd>
		<dt>4(进入维护模式)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="4" type="hidden">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>
		<dt>6[写入更新配置文件中特定的参数值
			<!-- ,格式：配置段.参数名=参数值 如 client.node_id=ABC,可以同时更新或写入多个参数，之间用;（分号）分割] -->
		</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="6" type="hidden">
				请输入配置: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>	
		<dt>11(更改工程密码，CMD为新的工程密码)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="11" type="hidden">
				密码: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>	
		<dt>12(更改登录密码，CMD为新的登录密码)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="12" type="hidden">
				密码: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>	
		<dt>14(重启)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="14" type="hidden">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>	
		<dt>15(更新mac地址白名单，CMD为列表，多个mac地址以逗号分割)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="15" type="hidden">
				MAC地址: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>	
		<dt>18(切换登录拦截功能状态，CMD为1时为开，CMD为0时为关)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="18" type="hidden">
				拦截功能状态：<select name="CMD">
					<option value="0">关</option>
					<option value="1">开</option>
				</select>
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>
		<dt>19(更改VPN服务器地址，CMD为  address:port:proto 格式)</dt>
		<dd>
			<form method="post" action="">
				<input name="CLID" value="{$arr.CLID}" type="hidden">
				<input name="CODE" value="19" type="hidden">
				VPN服务器地址: <input name="CMD">
				<input name="commit" value="ok" type="hidden">
				<input type="submit" value="提交">
			</form>
		</dd>									
	</dl>




</div>



</body>
</html>