<?php

class base{
	protected $timestamp = null;
	private $lang_default='zh-cn';
	private $device_default='web';
	protected $view = null;
	protected $redis = null;
	protected $CONFIG = null;
	
	function __construct(){
		$this->timestamp=get_utc_timestamp();
		$this->view = new Smarty();
		$this->redis= new Redis();
		$this->CONFIG=Application::get_config();
		$ret=$this->redis->connect($this->CONFIG['redis']['server'],$this->CONFIG['redis']['port']);
		if(!$ret){
			$this->err_message("Redis connect fail!");
		}
		
	}
	
	public function __get($name){
		switch ($name){
			case 'lang':
				return $this->get_lang();
				break;
			case 'device':
				return $this->get_device();
				break;
		}
	}
	
	
	/**
	 * 
	 * 输出不可恢复的错误信息
	 * 
	 * @param string $str 错误详情
	 * @public
	 * 
	 * @return bool
	 * @final
	 *
	 * @author zhufengme <i@zhufeng.me>
	 * @copyright Joome Inc.
	 * 
	 */
	final protected function err_message($str){
		header("HTTP/1.1 500 ".$str);
		echo "Error: $str";
		die();
	}
	
	
	/**
	 * 
	 * 得到用户当前的语言设定
	 * 
	 * @return string 符合ISO定义的语言描述字符串
	 * @public
	 * 
	 */
	public function get_lang(){
		if (!Application::in_page()){
			return $this->lang_default;
		}
		
		$user_choice_lang=get_value_from_array($_GET,'lang');
		if($user_choice_lang){
			if(in_array($user_choice_lang, $this->CONFIG['allow_langs'])){
				setcookie('lang',$user_choice_lang,$this->timestamp+60*60*24*30,"/");
				return $user_choice_lang;
			}
		}
		
		$user_choice_lang=get_value_from_array($_COOKIE,'lang');
		if($user_choice_lang){
			if(in_array($user_choice_lang, $this->CONFIG['allow_langs'])){
				setcookie('lang',$user_choice_lang,$this->timestamp+60*60*24*30,"/");
				return $user_choice_lang;
			}
		}
		
		$str=$_SERVER['HTTP_ACCEPT_LANGUAGE'];
		$str=strtolower($str);
		$arr=explode(",", $str);
		if(!$arr[0]){
			return $this->lang_default;
		}
		$brower_lang=$arr[0];
		foreach ($this->CONFIG['allow_langs'] as $lang){
			if (instr($brower_lang,$lang)){
				return $lang;
			}
		}
		return $this->lang_default;
	}

	/**
	 * 
	 * 得到用户当前的设备
	 * 
	 * @return string 设备名
	 * @public
	 * 
	 */
	public function get_device(){
		return 'web';
	}
	
	/**
	 * 
	 * 设定使用哪一组模版，并初始化模版引擎
	 * 
	 * @param string $page_type 模版名
	 * @param string $lang 用户语言（可选）
	 * @param string $device 用户设备（可选）
	 * 
	 * @return bool
	 * @final
	 *
	 * @author zhufengme <i@zhufeng.me>
	 * @copyright Joome Inc.
	 * 
	 */
	final protected function set_view($page_type,$lang=false,$device=false){
		if(!$lang){
			$lang=$this->get_lang();
		}
		if(!$device){
			$device=$this->get_device();
		}
				
		$tpls_path=TPL_PATH.$page_type;
		if(!file_exists($tpls_path)){
			$this->err_message("view type not found!");
			die();
		}
		
		if(file_exists($tpls_path.'/'.$lang)){
			$tpls_path.='/'.$lang;
		}else{
			$tpls_path.='/'.$this->lang_default;
		}
		$this->langs_file=$tpls_path."/langs.php";
		
		if(file_exists($tpls_path.'/'.$device)){
			$tpls_path.='/'.$device;
		}else{
			$tpls_path.='/'.$this->device_default;
		}
		
		$tpls_path.='/';
		$cache_path=CACHE_PATH.md5($tpls_path);
		$this->view->setTemplateDir($tpls_path);
		make_dir($cache_path."/compile/");
		$this->view->setCompileDir($cache_path."/compile/");
		make_dir($cache_path."/config/");
		$this->view->setConfigDir($cache_path."/config/");
		make_dir($cache_path."/cache/");
		$this->view->setCacheDir($cache_path."/cache/");
		return true;		
	}
	
	/**
	 * 
	 * 给模版中的变量赋值
	 * 
	 * @param mixed $name 变量名
	 * @param mixed $value 变量值
	 * @final
	 * 
	 * @author zhufengme <i@zhufeng.me>
	 * @copyright Joome Inc.
	 * 
	 */
	final protected function view_assign($name,$value){
		if($this->view===null){
			new Exception("View not be set!");
			die();
		}
		$this->view->assign($name,$value);
	}
	
	
	/**
	 * 
	 * 直接输出指定的模版
	 * 
	 * @param string $tpl_name 模版文件名
	 * @final
	 * 
	 * @author zhufengme <i@zhufeng.me>
	 * @copyright Joome Inc.
	 * 
	 */
	final protected function view_display($tpl_name){
		header("Content-Type: text/html; charset=utf-8");
		$html=$this->view_fetch($tpl_name);
		echo $html;
		return;
	}
	
	
	/**
	 * 
	 * 返回指定模版的html代码
	 * 
	 * @param string $tpl_name 模版文件名
	 * @final
	 * @return string html代码
	 * 
	 * @author zhufengme <i@zhufeng.me>
	 * @copyright Joome Inc.
	 * 
	 */
	final protected function view_fetch($tpl_name){
		if($this->view===null){
			new Exception("View not be set!");
			die();
		}
		require_once $this->langs_file;
		$this->view_assign('LANGS',$LANGS);
		$html = $this->view->fetch($tpl_name);
		return $html;
	}
	
}

