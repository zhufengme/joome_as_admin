<?php
class index extends controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function default_action(){
		$this->get_lang();
		header("Location: /admin");
		exit();
	}

}