<?php
/**
 * portal.class.php    portal控制类
 * @Author			sujiahui<sujiahui@joome.net>
 * @since:			2012-05-21
 * @CopyRight		Joome Inc.	
 */
class portal extends controller {
	public function __construct() {
		parent::__construct ();
	
	}
	
	/**
	 * AS到PS端的处理 指向portal鉴权页面
	 *
	 * @author sujiahui<sujiahui@joome.net>
	 * @since 2012-05-23
	 * @copyright Joome Inc.
	 */
	public function default_action() {
		// CMD 命令式 AUFW的时候
		if ($_GET ['CMD'] == "AUFW") {
			
			$psclapi = new psclapi ( $this->CONFIG ['api'] ['PID'], $this->CONFIG ['api'] ['PK'] );
			$_d = $_GET ['_d'];
			
			// 得到$_d的结果数组
			$arr_ret = $psclapi->AUFW ( $_d );
			
			// 取出其中的值
			$TSE = $arr_ret ['TSE'];
			$MCID = $arr_ret ['MCID'];
			$CLID = $arr_ret ['CLID'];
			$REF = $arr_ret ['REF'];
			// echo "clid:".$CLID."<br>";
			$this->load_model ( "user" );
			// 利用mcid判断是否已经是注册用户
			$as_mcid = $MCID;
			$check_user = user::get_uid_is_active ( $as_mcid );
			$owner = new user ();
			$owner_name = $owner->get_owner_name ( $CLID );
			$owner_words = $owner->get_owner_words ( $CLID );
			// 这里可以改成数据库提取
			// 设置用户登录时长
			$user_time = 10 * 60 * 60;
			$is_timeout = $owner->check_user_timeout ( $as_mcid, $user_time );
			// echo $is_timeout;
			// echo $owner_name;
			if ($check_user != 0) {
				$UID = user::get_uid_by_mcid ( $as_mcid );
				$user = new user ( $UID );
				
				if (is_object ( $user )) {
					
					$username = $user->get_username ();
					
					// 对象建立的用户进行操作
					$share_user_level = $user->get_user_level ( $CLID );
					$user_sum = $user->get_sum_users ( $CLID );
					$share_time = $user->get_total_time_by_clid ( $CLID );
					$user_onlinetimes = $user->get_user_online_times ( $username, $CLID );
					// 更新分享者信息
					$owner->update_owner_list ( $owner_name, $share_user_level, $share_time );
					//这里写一个方法进行 模板的切换 change_tpl
					$view_style=$this->change_tpl();
					
					$this->set_view ($view_style);
					$this->view_assign ( 'owner_name', $owner_name );
					$this->view_assign ( 'owner_words', $owner_words );
					$this->view_assign ( 'username', $username );
					$this->view_assign ( 'arr_ret', $arr_ret );
					$this->view_assign ( 'clid', base64_encode ( $CLID ) );
					$this->view_assign ( 'share_user_level', $share_user_level );
					$this->view_assign ( 'user_sum', $user_sum );
					$this->view_assign ( 'user_onlinetimes', $user_onlinetimes );
					$this->view_assign ( 'share_time', $share_time );
					$this->view_display ( 'index.tpl' );
					
					return;
				
				}
			} elseif ($check_user == 0 && $is_timeout == false) {
				$temp_user = user::get_user_by_as_mcid ( $as_mcid );
				$username = $temp_user->get_username ();
				$share_user_level = $temp_user->get_user_level ( $CLID );
				$user_sum = $temp_user->get_sum_users ( $CLID );
				$share_time = $temp_user->get_total_time_by_clid ( $CLID );
				$user_onlinetimes = $temp_user->get_user_online_times ( $username, $CLID );
				// 更新分享者信息
				$owner->update_owner_list ( $owner_name, $share_user_level, $share_time );
				//这里写一个方法进行 模板的切换 change_tpl
				$view_style=$this->change_tpl();
					
				$this->set_view ($view_style);
				$this->view_assign ( 'owner_name', $owner_name );
				$this->view_assign ( 'owner_words', $owner_words );
				$this->view_assign ( 'username', $username );
				$this->view_assign ( 'arr_ret', $arr_ret );
				$this->view_assign ( 'clid', base64_encode ( $CLID ) );
				$this->view_assign ( 'share_user_level', $share_user_level );
				$this->view_assign ( 'user_sum', $user_sum );
				$this->view_assign ( 'user_onlinetimes', $user_onlinetimes );
				$this->view_assign ( 'share_time', $share_time );
				$this->view_display ( 'index.tpl' );
				
				return;
			} elseif ($check_user == 0 && $is_timeout == true) {
				//这里写一个方法进行 模板的切换 change_tpl
				$view_style=$this->change_tpl();
					
				$this->set_view ($view_style);
				$this->view_assign ( 'as_mcid', base64_encode ( $as_mcid ) );
				// $this->view_assign ('_d', $d);
				$this->view_display ( "timeout.tpl" );
				return;
			}
		}
	}
	
	/**
	 * 用户点击免费上网 将参数传给AS
	 */
	public function user_login() {
		// 取得form表单的内容
		// 需要一个用户的ID
		// 通过分享者积分规则返回一个数组
		$TSE = $_POST ["TSE"];
		$TL = 7200;
		$URL = $_POST ["REF"];
		$QOSLEV = 1;
		$TOK = "";
		$arr = Array (
				'TSE' => $TSE,
				'TL' => $TL,
				'URL' => $URL,
				'QOSLEV' => $QOSLEV,
				'TOK' => $TOK 
		);
		// 生成一个api对象
		$psclapi = new psclapi ( $this->CONFIG ['api'] ['PID'], $this->CONFIG ['api'] ['PK'], $this->CONFIG ['api'] ['as_api'] );
		if (is_object ( $psclapi )) {
			// 将发起参数传给as
			$psclapi->AURT ( $arr );
		}
		
		return;
	}
	
	public function test() {
		
		$psclapi = new psclapi ( $this->CONFIG ['api'] ['PID'], $this->CONFIG ['api'] ['PK'] );
		$_d = $_GET ['_d'];
		
		// 得到$_d的结果数组
		$arr_ret = $psclapi->AUFW ( $_d );
		
		// 取出其中的值
		$TSE = $arr_ret ['TSE'];
		$MCID = $arr_ret ['MCID'];
		$CLID = $arr_ret ['CLID'];
		$REF = $arr_ret ['REF'];
		print_r ( $arr_ret );
		return;
	}
	/**
	 * 跳转到用户注册页面
	 */
	public function reg() {
		if (isset ( $_POST ["as_mcid"] )) {
			$as_mcid = $_POST ["as_mcid"];
			
		} elseif (isset ( $_GET ['as_mcid'] )) {
			$as_mcid = base64_decode ( $_GET ["as_mcid"] );
	
		}
		if (isset ( $_POST ["clid"] )){
			$clid = $_POST["clid"];
		}
		$login_mcid = $as_mcid;
			
		//这里写一个方法进行 模板的切换 change_tpl
		$view_style=$this->change_tpl();
					
		$this->set_view ($view_style);
		$this->view_assign ( "as_mcid", $as_mcid );
		$this->view_assign ( "clid", $clid );
		$this->view_assign ( "login_mcid", $login_mcid );
		$this->view_display ( 'reg.tpl' );
		return;
	}
	public function goto_login(){

		$as_mcid = $_POST ["as_mcid"];
		$clid=$_POST ['clid'];
		
		//这里写一个方法进行 模板的切换 change_tpl
		$view_style=$this->change_tpl();
			
		$this->set_view ($view_style);
		$this->view_assign ( "as_mcid", $as_mcid );
		$this->view_assign ( "clid", $clid );
		$this->view_display ( 'login.tpl' );
		return;
	}
	public function login(){
		$username = $_POST['username'];
		$password = $_POST['password'];
		$as_mcid = $_POST['as_mcid'];
		$CLID = $_POST['clid'];
/* 		echo $username."<br>";
		echo $password."<br>";
		echo $as_mcid;
		echo "<br>";
		echo $CLID; */
		$this->load_model ( "user" );
	 	$user=user::login($username, $password, $as_mcid);
		if (is_object ( $user )) {
			//写重定向任意网页返回到index页面
			header ( "Location:" . "http://baidu.com" );
			
			return;
			
		}
		else {
			//跳到登录失败页面
			return ;
		} 
	
	}
	/**
	 * 跳转到joome首页
	 */
	public function joome() {
		$this->set_view ( "lab", true );
		$this->view_display ( 'index.tpl' );
		return;
	}
	/**
	 * 用户不完全注册
	 */
	public function user_reg() {
		$username = $_POST ["username"];
		$password = $_POST ["password"];
		$email = $_POST ["email"];
		$code = $_POST ['code']; // OK代表已经同意
		$as_mcid = $_POST ["as_mcid"];
		$_d = $_POST ['_d'];
		$this->load_model ( "user" );
		$new_user = new user ();
		$reg_flag = $new_user->reg ( $username, $password, $email, $code, $as_mcid );
		if (is_object ( $reg_flag )) {
			// 这里写跳转到成功页面
			//这里写一个方法进行 模板的切换 change_tpl
			$view_style=$this->change_tpl();
					
			$this->set_view ($view_style);
			$this->view_display ( 'success.tpl' );
			return;
		}
		return;
	}
	
	/**
	 * 唯一性检查
	 */
	public function check_something() {
		
	//	$username = $_GET ['username'];
	
		$email = $_POST ['email'];
 		$username = $_POST['username'];
// 		$email =$_POST['email'];

		
		$this->load_model ( "user" );
		$user = new user ();
 		if ($username !=""){
 		$is_name=$user->user_name_check ( $username );		
 		}
		if ($email != ""){
			$is_email=$user->user_email_check ( $email );
		}
		$is_ok = Array(
				'username'=>$is_name,
				'email'=>$is_email
				);
		echo json_encode($is_ok);
	}
	
	/**
	 * 检测邮箱是否已经注册
	 */
	public function check_email_availability()
	{
		$email = $_POST ['email'];
		$this->load_model ( "user" );
		$user = new user ();
		if ($email != ""){
			$is_email=$user->user_email_check ( $email );
		}
		echo json_encode($is_email);
	}
	/**
	 * 检测用户名是否已经注册
	 */
	public function check_username_availability()
	{
		$username = $_POST['username'];
		$this->load_model ( "user" );
		$user = new user ();
		if ($username !=""){
			$is_name=$user->user_name_check ( $username );
		}
		echo json_encode($is_name);
	}

	
	/**
	 * 显示分享者信息
	 */
	public function show_owner_list() {
		$en_clid = $_GET ["clid"];
		$CLID = base64_decode ( $en_clid );
		$this->load_model ( "user" );
		$owner = new user ();
		$owner_name = $owner->get_owner_name ( $CLID );
		$owner_word = $owner->get_owner_words ( $CLID );
		$user_sum = $owner->get_sum_users ( $CLID );
		$owner_total_time = $owner->get_owner_total_time ( $CLID );
		$owner_level = $owner->get_owner_level ( $CLID );
		$TL = 3 * 60 * 60;
		
		$owner_power_arr = $owner->get_user_power ( $owner_level, $TL );
		$owner_tl = $this->second_to_hour ( $owner_power_arr ['TL'] );
		
		// 传递参数
		//这里写一个方法进行 模板的切换 change_tpl
		$view_style=$this->change_tpl();
					
		$this->set_view ($view_style);
		
		$this->view_assign ( "owner_name", $owner_name );
		$this->view_assign ( "owner_word", $owner_word );
		$this->view_assign ( "user_sum", $user_sum );
		$this->view_assign ( "owner_level", $owner_level );
		$this->view_assign ( "owner_total_time", $owner_total_time );
		$this->view_assign ( "owner_tl", $owner_tl );
		
		$this->view_display ( 'owner.tpl' );
		return;
	}
	/**
	 * 显示用户个人信息
	 */
	public function show_user_list() {
		$username = $_GET ['username'];
		$this->load_model ( "user" );
		$user = new user ();
		$user_level = 0;
		$user_tl = 3 * 60 * 60;
		$can_online_time_second = $user->get_user_power ( $user_level, $user_tl );
		$can_online_time = $this->second_to_hour ( $can_online_time_second ['TL'] );
		
		if ($user_level == 0) {
			$min_level = $user_level;
			$max_level = $user_level + 1;
		} elseif ($user_level == 10) {
			$min_level = $user_level - 1;
			$max_level = $user_level;
		} else {
			$min_level = $user_level - 1;
			$max_level = $user_level + 1;
		}
		
		// 传递参数
		//这里写一个方法进行 模板的切换 change_tpl
		$view_style=$this->change_tpl();
					
		$this->set_view ($view_style);
		
		$this->view_assign ( 'username', $username );
		$this->view_assign ( 'user_level', $user_level );
		$this->view_assign ( "can_online_time", $can_online_time );
		$this->view_assign ( 'min_level', $min_level );
		$this->view_assign ( 'max_level', $max_level );
		
		$this->view_display ( 'user.tpl' );
	
	}
	
	/**
	 * 将时间从秒转换为 天 时 分 的一个数组
	 *
	 * @param unknown_type $time
	 *        	时间
	 * @return array $time_arr
	 */
	private function time_change($time) {
		if ($time <= 60) {
			$day = 0;
			$hour = 0;
			$minute = $time;
		} elseif (($time > 60) && ($time <= 24 * 60)) {
			$day = 0;
			$hour = ($time - $time % 60) / 60;
			$minute = $time % 60;
		
		} else {
			$day = ($time - $time % 24) / 60;
			$time = $time - $day * 24;
			$hour = ($time - $time % 60) / 60;
			$minute = $time % 60;
		}
		
		$time_arr = Array (
				'day' => $day,
				'hour' => $hour,
				'minute' => $minute 
		);
		return $time_arr;
	}
	/**
	 * 把秒准换为小时
	 * 
	 * @param int $second
	 *        	秒
	 * @return int $hour 小时
	 */
	private function second_to_hour($second) {
		$hour = ($second - $second % 3600) / 3600;
		return $hour;
	
	}
	/**
	 * 终端适配引擎change_tpl
	 * @param unknown_type $agent 判断用户agent
	 * @return string $view_style 模版类型
	 */
	private function change_tpl(){
		$view_style = "phone_portal";
		$user_agent = Application::get_user_agent ();
		if(strpos($user_agent,"iPhone") || strpos($user_agent,"MIDP-2.0") || strpos($user_agent,"Opera Mini") || strpos($user_agent,"UCWEB") || strpos($user_agent,"Android")|| strpos($user_agent,"Windows Phone OS") )
		{
			$view_style = "phone_portal";
		}
		elseif (strpos($user_agent,"SymbianOS")){
			$view_style = "featurephone_portal";
		}
		elseif (strpos($user_agent,"ipad")){
			$view_style = "computer_portal";
		}
		else {
			$view_style = "computer_portal";
		}
		$view_style = "phone_portal";
		return $view_style;
		
	}
	

}