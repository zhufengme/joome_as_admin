<?php
/**
 * admin.class.php  admin控制类
 * @Author			sunwei<sunwei@joome.net>
 * @since:			2012-08-16
 * @CopyRight		Joome Inc.	
 */
include_once ("../PageSupport.class.php"); // 分页类
class admin extends controller {
	public function __construct(){
		parent::__construct();
	}

	public function default_action(){
		$this->index();
	}

	public function index(){		
		if(!$_SESSION["isLogin"]){
			//未登录
			header("Location: /admin/login");
		} else {
			$this->set_view("admin");
			$this->view_display("index.tpl");
		}
	}

	public function checklogin(){
		if(!$_SESSION["isLogin"]){
			header("Location: /admin/login");
		}
	}

	public function login(){
		//接受表单
		if(isset($_POST["sub"])){
			//处理传入参数
			$username = $_POST['username'];
			$password = $_POST['password'];
			//验证参数合法性
			if (empty($username)) {
				echo '<script type="text/javascript">alert("账号输入为空！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			} elseif (strlen($username) < 3 || strlen($username) > 15) {
				echo '<script type="text/javascript">alert("账号输入长度不规范！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			} elseif (!preg_match('/^[a-zA-Z][a-zA-Z0-9_.]{4,15}$/', $username)) {
				echo '<script type="text/javascript">alert("账号输入含有非法字符！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			}
			if (empty($password)) {
				echo '<script type="text/javascript">alert("密码输入为空！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			} else if((strlen($password) < 6 || strlen($password) > 16)){
				echo '<script type="text/javascript">alert("密码输入长度不规范！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			}	
			//实例化数据库操作类
			$this->load_model ( "adm" );
			$check = new adm();
			//验证登陆及引导
  			$checkresult = $check->checkadmin($username,$password);
			if($checkresult){
				$_SESSION["isLogin"] = true;
				$_SESSION["uid"] = $checkresult[0]['pid'];
				$_SESSION["username"] = $checkresult[0]['proxy_name'];
				echo "登陆成功！";
				echo '<script type="text/javascript">location.href="/"</script>';
			}else{
				echo '<script type="text/javascript">alert("账号或密码错误！")</script>';
				echo '<script type="text/javascript">location.href="/"</script>';
			}
		}
		$this->set_view("admin");
		$this->view_display("login.tpl");
		return;
	}

	public function top(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("top.tpl");
	}

	public function menu(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("menu.tpl");
	}
	public function menu2(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("menu2.tpl");
	}
	public function menu3(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("menu3.tpl");
	}	
	public function blank(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("blank.tpl");
	}
	public function main(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("main.tpl");
	}

	public function line(){
		$this->checklogin();
		$this->set_view("admin");
		$this->view_display("line.tpl");
	}

	public function lister() {
		//检查登陆状态
		$this->checklogin();
		//实例化后台数据库类
		$this->load_model ("adm");
		$get = new adm();
		//获取代理商列表
  		$get_proxy_list = $get->get_proxy_list();
  		//拼合代理商数组
  		$proxy_list_array = array();
  		foreach ($get_proxy_list as $key => $value) {
  				$proxy_list_array[$value['pid']] = $value['proxy_name'];
  		}
		//处理排序参数
		$opn = intval($_GET['opn']) == 1 ? 1 : '';//最近一次开机时间，取值范围1和空
		$last = intval($_GET['last']) == 1 ? 1 : '';//最近一次心跳时间，取值范围1和空
		$fm = intval($_GET['fm']) == 1 ? 1 : '';//剩余内存，取值范围1和空
		$ol = intval($_GET['ol']) == 1 ? 1 : '';//在线人数，取值范围1和空
		//组织排序数组
		$order_arr = array(
			'opn' => $opn,
			'last' => $last, 
			'fm' => $fm, 
			'ol' => $ol);
		//处理搜索参数
		$clid = $_GET['clid'];//节点编号，大写字母、数字和-
		$online = $_GET['online'];//数字
		$pid = intval($_GET['pid']);//代理商，数字
		//验证时间参数
		$onlinetimestart = get_utc_timestamp() - 60*30;
		$onlinetimeend = get_utc_timestamp();
		//实体化分页类
		$PAGE_SIZE = 10;
		$pageSupport = new PageSupport ( $PAGE_SIZE );

		$current_page = intval($_GET ["current_page"]);
		if (!empty($current_page)) {
			$pageSupport->set_current_page($current_page);
		} else {
			$pageSupport->set_current_page(1);
		}
		//拼sql
		if ($online == 1) {//在线
			if ($pid > 0) {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%' and A.LAST < '".$onlinetimeend."' and A.LAST > '".$onlinetimestart."' and C.pid = '".$pid."'";
			} else {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%' and A.LAST < '".$onlinetimeend."' and A.LAST > '".$onlinetimestart."'";				
			}
		}elseif ($online == 2) {//不在线
			if ($pid > 0) {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%' and A.LAST < '".$onlinetimestart."' and C.pid = '".$pid."'";
			} else {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%' and A.LAST < '".$onlinetimestart."'";				
			}			
		} else {//全部
			if ($pid > 0) {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%' and C.pid = '".$pid."'";
			} else {
				$sql = "select CLID,OPN,LAST,VER,FM,HW,ST,OL,WAN_RX,WAN_TX,CPULOAD,WDS,pid from nodes_status as A left join custom_nodes as B on A.CLID = B.node_id left join custom_list as C on B.custom_id = C.custom_id where A.CLID LIKE '%".$clid."%'";
			}
		}
		//排序
		foreach ($order_arr as $key => $value) {
			if ($value == 1) {
				$orderkey = $key;
			}
		}
		switch ($orderkey) {
			case 'opn':
				$sql = $sql."ORDER BY A.OPN DESC";//最近一次开机时间，时间最近
				break;
			case 'last':
				$sql = $sql."ORDER BY A.LAST DESC";//最近一次心跳时间，时间最近
				break;
			case 'fm':
				$sql = $sql."ORDER BY A.FM ASC";//剩余内存，内存最少
				break;
			case 'ol':
				$sql = $sql."ORDER BY A.OL DESC";//在线人数，人数最多
				break;
			default:
				break;
		}

		$pageSupport->set_sql ($sql);
		$pageSupport->read_data ();
		$result = $pageSupport->get_result ();
		if ($pageSupport->current_records > 0) 		// 如果数据不为空，则组装数据
		{
			//处理返回数组
			foreach ($result as $k => $v) {
				foreach ($v as $key => $value) {
					if (in_array($key, array('WAN_RX', 'WAN_TX'))) {
						if ($value > 1024*1024*1024*1024*8) {
							$result[$k][$key] = round($value/1024/1024/1024/1024/8, 2).'TB';
						} elseif ($value > 1024*1024*1024*8) {
							$result[$k][$key] = round($value/1024/1024/1024/8, 2).'GB';
						} elseif ($value > 1024*1024*8) {
							$result[$k][$key] = round($value/1024/1024/8, 2).'MB';
						} elseif ($value > 1024*8) {
							$result[$k][$key] = round($value/1024/8, 2).'KB';
						} else {
							$result[$k][$key] = $value.'bps';
						}
					} elseif ($key == 'pid') {
						$result[$k][$key] = $proxy_list_array[$value];
					} elseif ($key == 'LAST') {
						$mistime = get_utc_timestamp() - $value;
						if ($mistime < 60*30) {//30分钟以内
							$result[$k]['onlinestatus'] = '在线';	
						} else {
							$result[$k]['onlinestatus'] = '不在线';
							$result[$k]['OL'] = 'N/A';
							$result[$k]['FM'] = 'N/A';
							$result[$k]['CPULOAD'] = 'N/A';
							$result[$k]['WDS'] = 'N/A';
						}
					}
				}
			}

			$news_arr = $result;
		}

		$pageinfo_arr = array (
				'total_records' => $pageSupport->total_records,
				'current_page' => $pageSupport->current_page,
				'total_pages' => $pageSupport->total_pages,
				'first' => $pageSupport->first,
				'prev' => $pageSupport->prev,
				'next' => $pageSupport->next,
				'last' => $pageSupport->last 
		);
		$this->set_view ( "admin" );
		$this->view_assign('get_proxy_list', $get_proxy_list);
		$this->view_assign('opn', $opn);		
		$this->view_assign('last', $last);
		$this->view_assign('fm', $fm);
		$this->view_assign('ol', $ol);
		$this->view_assign('clid', $clid);
		$this->view_assign('online', $online);
		$this->view_assign('pid', $pid);
		$this->view_assign ( 'title', "CL状态" );
		$this->view_assign ( 'results', $news_arr );
		$this->view_assign ( 'pageSupport', $pageinfo_arr );
		$this->view_display ( 'lister.tpl' );		
	}

	public function control(){
		$this->checklogin();

		$referer_arr = array(
			'opn' => $_GET['opn'], 
			'last' => $_GET['last'], 
			'fm' => $_GET['fm'], 
			'ol' => $_GET['ol'], 
			'current_page' => $_GET['current_page'], 
			'clid' => $_GET['clid'], 
			'online' => $_GET['online'], 
			'pid' => $_GET['pid']);
		$referer = "http://admin.joome.info/admin/lister?".http_build_query($referer_arr);

		$arr = array();
		if (!empty($_POST) && $_POST['commit'] == 'ok') {
			$arr['CLID'] = $_POST['CLID'];
			$arr['CODE'] = $_POST['CODE'];//数字
			if ($_POST['CODE'] == 3 || $_POST['CODE'] == 11 || $_POST['CODE'] == 12) {
				$arr['CMD'] = $_POST['CMD'];//明文传入密码
			} else {
				$arr['CMD'] = $_POST['CMD'];
			}
			$arr['logtime'] = get_utc_timestamp();

			$this->load_model ("adm");
			$cmd = new adm();
  			$result = $cmd->control_insert($arr);
  			if ($result) {
  				echo '<script type="text/javascript">alert("操作成功!")</script>';
  			} else {
  				echo '<script type="text/javascript">alert("操作失败!")</script>';
  			}
		}

		$this->set_view ( "admin" );
		$this->view_assign("referer", $referer);
		$this->view_assign('arr', $_GET);
		$this->view_display ( 'control.tpl' );		
	}

	public function logout(){
		$username=$_COOKIE["username"];
		$sid=session_id();
		$_SESSION=array();
		if(isset($_COOKIE[session_name()])){
			setCookie(session_name(), '', time()-3600, '/');
		}
		session_destroy();
		header("Location: /");		
	}

	public function iptogeo(){
		set_time_limit(0);

		require_once('ip2locationlite.class.php');//转ip类

		//Load the class
		$ipLite = new ip2location_lite;
		$ipLite->setKey('450d551f4394d48260f454ca79a581bd42d7d123fadf4ed245d07d29e991b524');

		$this->load_model ("adm");
		$cmd = new adm();
		// $count_ip = $cmd->count_ip_nodes_status();
		// $nums = ceil($count_ip[0]['nums'] / 30);

		// for ($i=0; $i < $nums; $i++) { 
			$nodes_status_array = $cmd->get_ip_nodes_status(40);
			// echo "<pre>";
			// var_dump($nodes_status_array);
			// die;

			foreach ($nodes_status_array as $value) {
				print_r($value);
				echo "<br>";
				//Get errors and locations
				$locations = $ipLite->getCity($value['A_IP']);//$_SERVER['REMOTE_ADDR']

				print_r($locations);
				echo "<br>";

				if ($locations['statusCode'] == 'OK') {
					$result = $cmd->update_nodes_status($locations['latitude'], $locations['longitude'], '1',
					 $locations['countryCode'], $locations['countryName'], $locations['regionName'],
					 $locations['cityName'], $locations['timeZone'], $value['CLID']);



					if ($result) {
						echo $value['A_IP']."<br>";
					}
					sleep(2);
				}				
			}
		// }
	}

	public function uniquegeo(){
		// function getRandomPoint() {
		// 	var lat = 48.25 + (Math.random() - 0.5)*14.5;
		// 	var lng = 11.00 + (Math.random() - 0.5)*36.0;
		// 	return new GLatLng(Math.round(lat*10)/10, Math.round(lng*10)/10);
		// }

			$cache_file = 'cache/data.json';

			if(file_exists($cache_file)){
				require_once ($cache_file);
			}else{
				$this->load_model ("adm");
				$cmd = new adm();
				$all_geo = $cmd->unique_all_geo();	

				foreach ($all_geo as $k => $v) {
					if (substr($v['CLID'], 0, 2) == 'JM') {
						$all_geo[$k]['CLID'] = 'jm';
						$all_geo[$k]['type'] = '公共热点';
					} elseif (substr($v['CLID'], 0, 2) == 'WZ') {
						$all_geo[$k]['CLID'] = 'wz';
						$all_geo[$k]['type'] = '私人热点';
					} else {
						$all_geo[$k]['CLID'] = 'ot';
						$all_geo[$k]['type'] = '商业热点';
					}
				}

				// echo "<pre>";
				// print_r($all_geo);
				// die;

				$all_geo_array = array();
				foreach ($all_geo as $k => $v) {
					if ($v['sum'] == 1) {
						$all_geo_array[] = array('latitude' => $v['LAT'], 'longitude' => $v['LON'], 'joomeimg' => $v['CLID'], 'type' => $v['type'], 'online' => $v['OL']);
						unset($all_geo[$k]);
					} 
				}

				$newLAT = '';
				$newLON = '';
				foreach ($all_geo as $k => $v) {
					for ($i = 0; $i < $v['sum']; $i++) {
						$newLAT = $v['LAT'] + (rand(1,9999)/10000 - 0.5)*14.5*0.0001;
						$newLON = $v['LON'] + (rand(1,9999)/10000 - 0.5)*36.0*0.0001;
						$all_geo_array[] = array('latitude' => $newLAT, 'longitude' => $newLON, 'joomeimg' => $v['CLID'], 'type' => $v['type'], 'online' => $v['OL']);
					}
				}
				// print_r($all_geo_array);
				$reset_array = array('count'=>count($all_geo_array), 'photos'=>$all_geo_array);
				$tmp .= "var data = ".json_encode($reset_array);
				$fp = @fopen($cache_file,'w+');
				@fwrite($fp,$tmp);
				@fclose($fp);
				require_once ($cache_file);
			}
		echo file_get_contents($all_geo_cache);
		die;		




				// echo "<br><br><br><br>";
				// print_r($all_geo_array);

				// foreach($all_geo as $k=>$v){
				// 	if(isset($all_geo[$v]) && $all_geo[$v]==$k)
				// 	unset($all_geo[$v]);
				// }

				// echo "<br><br><br><br>";
				// print_r($all_geo);






	}

	public function newuniquegeo(){
		header("Content-type: application/json");
		$cache_file = 'cache/data.json';

		if(file_exists($cache_file)){
			$timezone = "Asia/Chongqing";
			if (function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
			$cache_file_time = filemtime($cache_file);
			$now_time = time();
			$time_interval = abs($cache_file_time - $now_time);
			if (($time_interval > 60*10)) {
				unlink($cache_file);
				if (!file_exists($cache_file)) {
					$this->load_model ("adm");
					$cmd = new adm();
					$all_geo = $cmd->new_unique_all_geo();	

					foreach ($all_geo as $k => $v) {
						if ($v['CINAME'] == 'BEIJING') {
							$all_geo[$k]['LAT'] = '39.923447';
							$all_geo[$k]['LON'] = '116.518082';
						}
						$all_geo[$k]['CINAME'] = $this->ciname_to_chs($v['CINAME']);
						if ((get_utc_timestamp() - $v['LAST']) < 60*30) {
							$all_geo[$k]['LAST'] = '在线';
						} else {
							$all_geo[$k]['LAST'] = '不在线';
						}
						if ($v['ST'] == 'JM') {
							$all_geo[$k]['ST'] = 'jm';
							$all_geo[$k]['type'] = '公共热点';
						} elseif ($v['ST'] == 'WZ') {
							$all_geo[$k]['ST'] = 'wz';
							$all_geo[$k]['type'] = '私人热点';
						} else {
							$all_geo[$k]['ST'] = 'ot';
							$all_geo[$k]['type'] = '商业热点';
						}
					}

					$all_geo_array = array();
					$newLAT = '';
					$newLON = '';
					foreach ($all_geo as $k => $v) {
							$newLAT = $v['LAT'] + (rand(1,9999)/10000 - 0.5)*14.5*0.0001;
							$newLON = $v['LON'] + (rand(1,9999)/10000 - 0.5)*36.0*0.0001;
							$all_geo_array[] = array(
								'latitude' => $newLAT, //经度
								'longitude' => $newLON, //纬度
								'joomeimg' => $v['ST'], //图片类型
								'city' => $v['CINAME'],//城市
								'type' => $v['type'], //热点类型
								'last' => $v['LAST'],//在线状态
								'online' => $v['OL']//在线人数
								);
					}
					$reset_array = array('count'=>count($all_geo_array), 'photos'=>$all_geo_array);
					$tmp .= "var data = ".json_encode($reset_array);
					$fp = @fopen($cache_file,'w+');
					@fwrite($fp,$tmp);
					@fclose($fp);
					require_once ($cache_file);
				}
			} else {
				require_once ($cache_file);
			}
		}else{
			$this->load_model ("adm");
			$cmd = new adm();
			$all_geo = $cmd->new_unique_all_geo();	

			foreach ($all_geo as $k => $v) {
				if ($v['CINAME'] == 'BEIJING') {
					$all_geo[$k]['LAT'] = '39.923447';
					$all_geo[$k]['LON'] = '116.518082';
				}
				$all_geo[$k]['CINAME'] = $this->ciname_to_chs($v['CINAME']);
				if ((get_utc_timestamp() - $v['LAST']) < 60*30) {
					$all_geo[$k]['LAST'] = '在线';
				} else {
					$all_geo[$k]['LAST'] = '不在线';
				}
				if ($v['ST'] == 'JM') {
					$all_geo[$k]['ST'] = 'jm';
					$all_geo[$k]['type'] = '公共热点';
				} elseif ($v['ST'] == 'WZ') {
					$all_geo[$k]['ST'] = 'wz';
					$all_geo[$k]['type'] = '私人热点';
				} else {
					$all_geo[$k]['ST'] = 'ot';
					$all_geo[$k]['type'] = '商业热点';
				}
			}

			$all_geo_array = array();
			$newLAT = '';
			$newLON = '';
			foreach ($all_geo as $k => $v) {
					$newLAT = $v['LAT'] + (rand(1,9999)/10000 - 0.5)*14.5*0.0001;
					$newLON = $v['LON'] + (rand(1,9999)/10000 - 0.5)*36.0*0.0001;
					$all_geo_array[] = array(
						'latitude' => $newLAT, //经度
						'longitude' => $newLON, //纬度
						'joomeimg' => $v['ST'], //图片类型
						'city' => $v['CINAME'],//城市
						'type' => $v['type'], //热点类型
						'last' => $v['LAST'],//在线状态
						'online' => $v['OL']//在线人数
						);
			}

			$reset_array = array('count'=>count($all_geo_array), 'photos'=>$all_geo_array);
			$tmp .= "var data = ".json_encode($reset_array);
			$fp = @fopen($cache_file,'w+');
			@fwrite($fp,$tmp);
			@fclose($fp);
			require_once ($cache_file);
		}

		return;
	}	

	public function ciname_to_chs($eng_name){
		$chs_name = '';
		switch ($eng_name) {
			case 'CHENGDU':
				$chs_name = '成都';
				break;
			case 'WUHAN':
				$chs_name = '武汉';
				break;
			case 'SHENYANG':
				$chs_name = '沈阳';
				break;
			case 'HEFEI':
				$chs_name = '合肥';
				break;
			case 'SHIJIAZHUANG':
				$chs_name = '石家庄';
				break;
			case 'NANNING':
				$chs_name = '南京';
				break;
			case 'CHONGQING':
				$chs_name = '重庆';
				break;																								
			case 'GUILIN':
				$chs_name = '桂林';
				break;
			case 'HANGZHOU':
				$chs_name = '杭州';
				break;
			case 'TIANJIN':
				$chs_name = '天津';
				break;
			case 'SHANGHAI':
				$chs_name = '上海';
				break;
			case 'NANJING':
				$chs_name = '南京';
				break;
			case 'GUANGZHOU':
				$chs_name = '贵州';
				break;
			case 'XIAMEN':
				$chs_name = '厦门';
				break;
			case 'FUZHOU':
				$chs_name = '福州';
				break;
			case 'BEIJING':
				$chs_name = '北京';
				break;
			default:
				$chs_name = $eng_name;
				break;
		}
		return $chs_name;
	}

	public function iptogeonew(){
		ob_end_flush();//关闭缓存
		set_time_limit(0);
		ob_implicit_flush();//强制每当有输出就自动刷新，相当于在每个echo后，调用flush()
		for($i=0;$i<10;$i++){
			echo "Now Index is :". $i;
			echo "<br>";
			// flush();
			sleep(1);
		}






	}

	public function lbstest(){



		// ini_set("precision", 49);
		// echo pi();

	}

	public function map(){

			$cache_file = 'cache/all_geo_cache.php';

			if(file_exists($cache_file)){
				require_once ($cache_file);
			}else{
				$this->load_model ("adm");
				$cmd = new adm();
				$all_geo = $cmd->get_all_geo();				
				$tmp .= "<?php \r\n";
				$tmp .= '$all_geo_cache = array('."\r\n";
				if(!empty($all_geo)){
					foreach ($all_geo as $k=>$v){
						$tmp .= "\t\t'".$k."'=> array('LAT'=>".$v['LAT'].", 'LON'=>".$v['LON'].") ,\r\n";
					}
				}
				$tmp .= ');';
				$fp = @fopen($cache_file,'w+');
				@fwrite($fp,$tmp);
				@fclose($fp);
				require_once ($cache_file);
			}
		echo json_encode($all_geo_cache);
		die;
	}

	public function mapdata(){
			$cache_file = 'cache/data.json';

			if(file_exists($cache_file)){
				require_once ($cache_file);
			}else{
				$this->load_model ("adm");
				$cmd = new adm();
				$all_geo = $cmd->get_all_geo();	
				$new_all_geo = array();
				foreach ($all_geo as $k => $v) {
					foreach ($v as $key => $value) {
						if ($key == 'LAT') {
							$new_all_geo[$k]['latitude'] = $value;
						} elseif ($key == 'LON') {							
							$new_all_geo[$k]['longitude'] = $value;
						}
					}
				}	
				$reset_array = array('count'=>count($new_all_geo), 'photos'=>$new_all_geo);
				$tmp .= "var data = ".json_encode($reset_array);
				$fp = @fopen($cache_file,'w+');
				@fwrite($fp,$tmp);
				@fclose($fp);
				require_once ($cache_file);
			}
		echo file_get_contents($all_geo_cache);
		die;		
	}

	public function mapview(){
		$this->set_view ( "admin" );
		$this->view_assign("wait", $wait);
		$this->view_display ( 'map.tpl' );
	}


	public function statuscount(){
		//初始化统计店家
		$user_online = '1';
		$clid = $_GET['clid'];
		$clid_arr = explode(",", $clid);
		$clid_str = "";
		foreach ($clid_arr as $v) {
			$clid_str .= "'".$v."',";
		}
		$clid_str = rtrim($clid_str, ",");
		$date = $_GET['date'];
		$this->view_assign('clid', $clid);
		$this->view_assign('date', $date);
		//初始化数据库类
		$this->load_model ("adm");
		$cmd = new adm();
		//获取cl列表
		$sql = "SELECT A.CLID,A.UID,A.cl_name,A.timezone,A.seat_num,A.seat_coefficient,A.office_hours,B.CLID,B.STATUS FROM t_cls_list as A left join t_cls_status as B on A.CLID = B.CLID WHERE A.is_on = 1";
		$cls_arr = $cmd->status_count($sql);
		$this->view_assign("cls_arr", $cls_arr);

		$ggg = "";
		$hhh = "";
		$office_hours = "";
		$timezone = "";
		foreach ($cls_arr as $k => $v) {
			if ($v['CLID'] == $clid) {
				$ggg = $v['seat_coefficient'];
				$hhh = $v['seat_num'];
				$office_hours = $v['office_hours'];
				$timezone = $v['timezone'];
			}
		}


		$this->view_assign('ggg', $ggg);
		$this->view_assign('hhh', $hhh);

		$office_hours_arr = explode("-", $office_hours);

		$office_hours_start = preg_replace("/:[0-9]+/", "", $office_hours_arr[0]);
		$office_hours_end = preg_replace("/0/", "", preg_replace("/:[0-9]+/", "", $office_hours_arr[1]));

		if ($office_hours_end < 7) {
			$office_hours_end = 24 + $office_hours_end;
		}

		echo "系统时区 ".date_default_timezone_get() . "<br />";
		$timezone = "Asia/Chongqing";
		echo "变更时区为 ".$timezone."<br />";

		if (function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
		echo "检查时区 " .date_default_timezone_get() . "<br />";

		$start_time = strtotime($date);
		$end_time = strtotime($date)+86400;

		echo "UTC时间 " .date('Y-m-d H:i:s ', get_utc_timestamp())."<br />";
		echo "现在时间 ".date('Y-m-d H:i:s')."<br />";
		echo "统计开始于 " .date('Y-m-d H:i:s ', $start_time)."<br />";
		echo "统计结束于 " .date('Y-m-d H:i:s ', $end_time)."<br />";
		echo "------检查时区结束------<br /><br />";
		echo "当前查询时间为 ".date('Y-m-d H:i:s', $_GET['ctime']);

		$t_str = "";
		for ($i=$office_hours_start; $i < ($office_hours_end+1); $i++) { 
			$t_str = "t_".$i;
			$$t_str = $start_time + 3600*$i;
		}

//停留人次
		$sql = "SELECT UID FROM `t_online_history` where CLID in (".$clid_str.") and logtime > '".$start_time."' and logtime < '".$end_time."' and user_online = '".$user_online."' group by UID order by UID asc";
		// echo $sql."<br />";
		$result = $cmd->status_count($sql);
		$aaa = count($result);
		// echo $aaa."<br />";
		$this->view_assign("aaa", $aaa);		

//平均停留
		$sql = "SELECT sum(total_time) as alltime FROM `t_online_history` where CLID in (".$clid_str.") and logtime > ".$start_time." and logtime < ".$end_time." and user_online = '".$user_online."'";
		// echo $sql."<br />";
		$result = $cmd->status_count($sql);
		$bbb = number_format($result[0]['alltime']/$aaa/60, 1);
		$this->view_assign("bbb", $bbb);
//计算每小时的在线人数
		$t_str = "";
		$t_str_1 = "";
		$sql = "";
		$c_arr = array();
		for ($i=$office_hours_start; $i < ($office_hours_end+1); $i++) {
			$t_str = "t_".$i;
			$t_str_1 = "t_".($i+1);
			$sql = "SELECT UID FROM `t_online_history` where CLID in (".$clid_str.") and logtime > '".$$t_str."' and logtime < '".$$t_str_1."' and user_online = '".$user_online."' group by UID order by UID asc";
			// echo $sql."<br />";
			$r_str = "c_".$i;
			$result = $cmd->status_count($sql);
			$c_arr[$i] = count($result);
		}
		asort($c_arr);
		
		$k = '';
		$v = '';
		foreach ($c_arr as $key => $value) {
			$k = $key;
			$v = $value;
		}
		$this->view_assign("ccc", $k);		
		$this->view_assign("ddd", $v);		

		//以往
		$sql = "SELECT MCID FROM `t_online_history` WHERE CLID IN (".$clid_str.") AND logtime < '".$start_time."' AND user_online = '".$user_online."' GROUP BY MCID ORDER BY MCID ASC";
		$mcid_arr_before = $cmd->status_count($sql);

		$mcid_arr_before_fe = array();
		foreach ($mcid_arr_before as $key => $value) {
			$mcid_arr_before_fe[$key] = $value['MCID'];
		}

		//今天
		$sql = "SELECT MCID FROM `t_online_history` WHERE CLID IN (".$clid_str.") AND logtime > '".$start_time."' AND logtime < '".$end_time."' AND user_online = '".$user_online."' GROUP BY MCID ORDER BY MCID ASC";
		$mcid_arr_today = $cmd->status_count($sql);

		$mcid_arr_today_fe = array();
		foreach ($mcid_arr_today as $key => $value) {
			$mcid_arr_today_fe[$key] = $value['MCID'];
		}
		$mcid_arr_diff = array_diff($mcid_arr_today_fe, $mcid_arr_before_fe);
//新顾客
		$new_customer_num = count($mcid_arr_diff);
//回头客
		$old_customer_num = count($mcid_arr_today_fe) - $new_customer_num;

		$this->view_assign("eee", $new_customer_num);
		$this->view_assign("fff", $old_customer_num);
		$this->set_view('admin');
		$this->view_display('statuscount.tpl');
	}











		//latitude 纬度 LAT
		//longitude 经度 LON
		//isgeo 是否有经纬度信息 ISGEO

// ALTER TABLE `nodes_status` ADD `LAT` VARCHAR( 15 ) NOT NULL COMMENT '纬度',
// ADD `LON` VARCHAR( 15 ) NOT NULL COMMENT '经度',
// ADD `ISGEO` VARCHAR( 5 ) NOT NULL COMMENT '是否有经纬度信息'		

// ALTER TABLE `nodes_status` ADD `CCODE` VARCHAR( 15 ) NOT NULL 

// ALTER TABLE `nodes_status` ADD `CNAME` VARCHAR( 15 ) NOT NULL ,
// ADD `RNAME` VARCHAR( 15 ) NOT NULL ,
// ADD `CINAME` VARCHAR( 15 ) NOT NULL ,
// ADD `TZONE` VARCHAR( 15 ) NOT NULL 

// ALTER TABLE `nodes_status` ADD `LATLON` VARCHAR( 15 ) NOT NULL 
/*

$result = $cmd->update_nodes_status(
$locations['latitude'], 
$locations['longitude'],
  '1',
$locations['countryCode'], 
$locations['countryName'], 
$locations['regionName'],
$locations['cityName'],
$locations['timeZone'],
  $value['CLID']);

echo $sql = "UPDATE nodes_status SET 
LAT = '".$LAT."', $locations['latitude'],
LON = '".$LON."', $locations['longitude'],
ISGEO = '".$IS_GEO."',  1
CCODE = '".$CCODE."', $locations['countryCode'], 
CNAME = '".$CNAME."', $locations['countryName'], 
RNAME = '".$RNAME."', $locations['regionName'],
CINAME = '".$CINAME."', $locations['cityName'],
TZONE = '".$TZONE."',  $locations['timeZone'],
LATLON = '".$LAT.",".$LON."' 经纬度连接串
WHERE CLID = '".$CLID."'";					 

array(11) {
  ["statusCode"]=>
  string(2) "OK"
  ["statusMessage"]=>
  string(0) ""
  ["ipAddress"]=>
  string(13) "49.76.212.159"
  ["countryCode"]=>
  string(2) "CN"
  ["countryName"]=>
  string(5) "CHINA"
  ["regionName"]=>
  string(7) "JIANGSU"
  ["cityName"]=>
  string(7) "NANJING"
  ["zipCode"]=>
  string(1) "-"
  ["latitude"]=>
  string(7) "32.0617"
  ["longitude"]=>
  string(7) "118.778"
  ["timeZone"]=>
  string(6) "+08:00"
}

*/		
		 
		//Getting the result
		// echo "<p>\n";
		// echo "<strong>First result</strong><br />\n";
		// if (!empty($locations) && is_array($locations)) {
		//   foreach ($locations as $field => $val) {
		//     echo $field . ' : ' . $val . "<br />\n";
		//   }
		// }
		// echo "</p>\n";
		 
		// //Show errors
		// echo "<p>\n";
		// echo "<strong>Dump of all errors</strong><br />\n";
		// if (!empty($errors) && is_array($errors)) {
		//   foreach ($errors as $error) {
		//     echo var_dump($error) . "<br /><br />\n";
		//   }
		// } else {
		//   echo "No errors" . "<br />\n";
		// }
		// echo "</p>\n";




	
	
	
}