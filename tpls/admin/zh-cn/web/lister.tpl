{*
	{$pageSupport.total_records}<br/>
	{$pageSupport.current_page}<br/>
	{$pageSupport.total_pages}<br/>
	{$pageSupport.first}<br/>
	{$pageSupport.prev}<br/>
	{$pageSupport.next}<br/>
	{$pageSupport.last}<br/>
*}
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$title}</title>
</head>
<body>
<form action="" method="get">
节点编号(CLID): <input type="text" name="clid" value="{$clid}" />
是否在线：<select name="online">
<option value="0"{if ($online == 0)} selected="selected"{/if}>全部</option>
<option value="1"{if ($online == 1)} selected="selected"{/if}>在线</option>
<option value="2"{if ($online == 2)} selected="selected"{/if}>不在线</option>
</select>
代理商：<select name="pid">
<option value="0"{if ($pid == 0)} selected="selected"{/if}>全部</option>
{section loop=$get_proxy_list name=ls}
<option value="{$get_proxy_list[ls].pid}"{if ($pid == $get_proxy_list[ls].pid)} selected="selected"{/if}>{$get_proxy_list[ls].proxy_name}</option>
{/section}
</select>


<input type="submit" value="GO"/>
<a href="/admin/lister">重置搜索条件</a>


<table align="center" border="1" width=900>
	<caption><h1>{$title}</h1></caption>

	<tr>
		
		<th>节点编号(CLID)</th>
		<th><a href="?opn=1&last=&fm=&ol=&current_page={$pageSupport.current_page}&clid={$clid}&online={$online}&pid={$pid}">最近一次开机时间(OPN)</a></th>
		<th><a href="?opn=&last=1&fm=&ol=&current_page={$pageSupport.current_page}&clid={$clid}&online={$online}&pid={$pid}">最近一次心跳时间(LAST)</a></th>
		<th>当前固件版本(VER)</th>
		<th><a href="?opn=&last=&fm=1&ol=&current_page={$pageSupport.current_page}&clid={$clid}&online={$online}&pid={$pid}">剩余内存(FM)</a></th>
		<th>硬件类别(HW)</th>
		<th>软件类别(ST)</th>
		<th><a href="?opn=&last=&fm=&ol=1&current_page={$pageSupport.current_page}&clid={$clid}&online={$online}&pid={$pid}">在线人数(OL)</a></th>
		<th>WAN端口接收流量(WAN_RX)</th>
		<th>WAN端口发送流量(WAN_TX)</th>
		<th>CPU负荷(CPULOAD)</th>
		<th>WDS状态(WDS)</th>
		<th width="250">在线状态</th>
		<th>代理商ID(pid)</th>
		<th>操作</th>
<!-- 		<th>国内网站到达速度(SPCN)</th>
		<th>PS_API到达速度(SPPS)</th>
		<th>被GFW网站到达速度(SPBL)</th>
		<th>海外网站到达速度(SPOT)</th>
		<th>到达AS_API的速度(SPAS)</th>
		<th>mac地址白名单(MACWL)</th>
		<th>CL_API地址(CL_API)</th>
		<th>CL的接入IP地址(A_IP)</th>
		<th>CL的WAN口IP(W_IP)</th> -->
	</tr>
	{section loop=$results name=ls}
		<tr>
			<td>{$results[ls].CLID}</td>
			<td>{date('Y-m-d H:i:s', $results[ls].OPN+3600*8)}</td>
			<td>{date('Y-m-d H:i:s', $results[ls].LAST+3600*8)}</td>
			<td>{$results[ls].VER}</td>
			<td>{$results[ls].FM}</td>
			<td>{$results[ls].HW}</td>
			<td>{$results[ls].ST}</td>
			<td>{$results[ls].OL}</td>
			<td>{$results[ls].WAN_RX}</td>
			<td>{$results[ls].WAN_TX}</td>
			<td>{$results[ls].CPULOAD}</td>
			<td>{$results[ls].WDS}</td>
			<th>{$results[ls].onlinestatus}</th>
			<th>{$results[ls].pid}</th>
			<th width="200"><a href="/admin/control?{http_build_query($results[ls])}&opn={$opn}&last={$last}&fm={$fm}&ol={$ol}&current_page={$pageSupport.current_page}&clid={$clid}&online={$online}&pid={$results[ls].pid}">点击操作</a></th>
<!-- 			<td>{$results[ls].SPCN}</td>
			<td>{$results[ls].SPPS}</td>
			<td>{$results[ls].SPBL}</td>
			<td>{$results[ls].SPOT}</td>
			<td>{$results[ls].SPAS}</td>
			<td>{$results[ls].MACWL}</td>
			<td>{$results[ls].CL_API}</td>
			<td>{$results[ls].A_IP}</td>
			<td>{$results[ls].W_IP}</td>
 -->		</tr>
	{sectionelse}
		<tr>
			<td colspan="12">没有数据</td>
		</tr>
	{/section}
</table>
    <br>   


<br/>




	
	共{$pageSupport.total_records}记录
	第{$pageSupport.current_page}页/共{$pageSupport.total_pages}页
	{if ( $pageSupport.current_page > 1 )}
	   <A href="?current_page={$pageSupport.first}&clid={$clid}&online={$online}&pid={$pid}&opn={$opn}&last={$last}&fm={$fm}&ol={$ol}">首页</A>
	   <A href="?current_page={$pageSupport.prev}&clid={$clid}&online={$online}&pid={$pid}&opn={$opn}&last={$last}&fm={$fm}&ol={$ol}">上一页</A>
	{/if}
	
	{if ( $pageSupport.current_page < $pageSupport.total_pages )}
	   <A href="?current_page={$pageSupport.next}&clid={$clid}&online={$online}&pid={$pid}&opn={$opn}&last={$last}&fm={$fm}&ol={$ol}">下一页</A>
	   <A href="?current_page={$pageSupport.last}&clid={$clid}&online={$online}&pid={$pid}&opn={$opn}&last={$last}&fm={$fm}&ol={$ol}">末页</A>
	{/if}
	
	  跳到<input type="text" size="4" name="current_page" value="{$pageSupport.current_page}"/>页
	  <input type="submit" value="GO"/>
	</form>




</body>
</html>