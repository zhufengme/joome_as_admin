<?php
/**
 * 
 * 定位类
 * @author sunwei@joome.net
 *
 */
class adm extends model {

	public function __construct() {
		parent::__construct ();
	}

	/**
	 * 经纬度范围计算函数(单位：米)
	 * 
	 * @param int $lat 纬度
	 * @param int $lng 经度
	 * @param int $raidus 半径
	 * @return array (minlat,maxlat,minlng,maxlng)经纬度范围
	 */
	function getaround($lat,$lng,$raidus){
		$pi = 3.1415926;
		$degree = (24901*1609)/360.0;
		$dpmlat = 1/$degree;
		$raiduslat = $dpmlat*$raidus;
		$minlat = $lat-$raiduslat;
		$maxlat = $lat+$raiduslat;
		
		$mpdlng = $degree*cos($lat*($pi/180));
		$dpmlng = 1/$mpdlng;
		$raiduslng = $dpmlng*$raidus;
		$minlng = $lng-$raiduslng;
		$maxlng = $lng+$raiduslng;
		return array($minlat,$maxlat,$minlng,$maxlng);
	}
	/**
	 * 计算两个坐标之间的距离（单位：千米）
	 * 
	 * @param $lat1,$lng1,$lat2,$lng2(两个坐标的经纬度)
	 * @return $s距离
	 */
	function rad($d){  
	    return $d * 3.1415926535898 / 180.0;  
	} 
	function getdistance($lat1, $lng1, $lat2, $lng2){
		$EARTH_RADIUS = 6378.137;  
		$radLat1 = rad($lat1);  
		$radLat2 = rad($lat2);  
		$a = $radLat1 - $radLat2;  
		$b = rad($lng1) - rad($lng2);  
		$s = 2 * asin(sqrt(pow(sin($a/2),2) +  
		cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));  
		$s = $s *$EARTH_RADIUS;  
		$s = round($s * 10000) / 10000;  
		return $s;  
	}

}











