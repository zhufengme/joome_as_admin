/**
 * @fileoverview This demo is used for MarkerClusterer. It will show 100 markers
 * using MarkerClusterer and count the time to show the difference between using
 * MarkerClusterer and without MarkerClusterer.
 * @author Luke Mahe (v2 author: Xiaoxi Wu)
 */

function $(element) {
  return document.getElementById(element);
}

var speedTest = {};

speedTest.pics = null;
speedTest.map = null;
speedTest.markerClusterer = null;
speedTest.markers = [];
speedTest.infoBubble = null;

speedTest.init = function() {
  var latlng = new google.maps.LatLng(32.91, 105.38);
  var options = {
    'zoom': 5,
    'center': latlng,
    'mapTypeId': google.maps.MapTypeId.ROADMAP
  };

  speedTest.map = new google.maps.Map($('map'), options);
  speedTest.pics = data.photos;
  
  var useGmm = document.getElementById('usegmm');
  google.maps.event.addDomListener(useGmm, 'click', speedTest.change);
  
  var numMarkers = document.getElementById('nummarkers');
  google.maps.event.addDomListener(numMarkers, 'change', speedTest.change);

  speedTest.infoBubble = new InfoBubble({
    shadowStyle: 0,
    borderWidth: 0,
    backgroundColor: 'transparent',
    arrowSize: 0
  });

  speedTest.showMarkers();
};

speedTest.showMarkers = function() {
  speedTest.markers = [];

  var type = 1;
  if ($('usegmm').checked) {
    type = 0;
  }

  if (speedTest.markerClusterer) {
    speedTest.markerClusterer.clearMarkers();
  }

  var panel = $('markerlist');
  panel.innerHTML = '';
  var numMarkers = data.count;//$('nummarkers').value;

  for (var i = 0; i < numMarkers; i++) {
    var titleText = speedTest.pics[i].photo_title;
    if (titleText == '') {
      titleText = 'No title';
    }

    var item = document.createElement('DIV');
    var title = document.createElement('A');
    title.href = '#';
    title.className = 'title';
    title.innerHTML = titleText;

    item.appendChild(title);
    panel.appendChild(item);


    var latLng = new google.maps.LatLng(speedTest.pics[i].latitude,
        speedTest.pics[i].longitude);

    var JooMeIcons = [];
    JooMeIcons['wz'] = '../img/map/marker.png';//红色
    JooMeIcons['jm'] = '../img/map/marker.png';//绿色
    JooMeIcons['ot'] = '../img/map/marker.png';//蓝色

    var markerImage = new google.maps.MarkerImage(JooMeIcons[speedTest.pics[i].joomeimg], new google.maps.Size(34, 42));

    var marker = new google.maps.Marker({
      'position': latLng,
      'icon': markerImage
    });

    var fn = speedTest.markerClickFunction(speedTest.pics[i], latLng);
    google.maps.event.addListener(marker, 'click', fn);
    google.maps.event.addDomListener(title, 'click', fn);
    google.maps.event.addDomListener(map, 'click', function() {
          speedTest.infoBubble.close();
        });
    speedTest.markers.push(marker);
  }

  window.setTimeout(speedTest.time, 0);
};

speedTest.markerClickFunction = function(pic, latlng) {
  return function(e) {
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
      e.stopPropagation();
      e.preventDefault();
    }
    var title = pic.photo_title;
    var url = pic.photo_url;
    var fileurl = pic.photo_file_url;

    // var infoHtml = '<div class="info"><h3>' + title +
    //   '</h3><div class="info-body">' +
    //   '<a href="' + url + '" target="_blank"><img src="' +
    //   fileurl + '" class="info-img"/></a></div>' +
    //   '<a href="http://www.panoramio.com/" target="_blank">' +
    //   '<img src="http://maps.google.com/intl/en_ALL/mapfiles/' +
    //   'iw_panoramio.png"/></a><br/>' +
    //   '<a href="' + pic.owner_url + '" target="_blank">' + pic.owner_name +
    //   '</a></div></div>';

  // var infoHtml = pic.city + '<br />' + pic.type + '<br />' + pic.last + '<br />';

  var typestyle;
  if (pic.type == "私人热点") {
    typestyle = 'personal_wifi';
  } else if (pic.type == "公共热点") {
    typestyle = 'public_wifi';
  } else if (pic.type == "商业热点") {
    typestyle = 'business_wifi';
  }

  var infoHtml ='<div id="info" class="' + typestyle + '"><div id="type">' + pic.type +'</div><div id="location">' + pic.city + '</div><div id="status">' + pic.last + '</div><div id="people">' + pic.online + '</div></div>';

  // if (pic.last == '在线') {
  // infoHtml = infoHtml + onlineinfo;
  // };

    speedTest.infoBubble.setContent(infoHtml);
    speedTest.infoBubble.setPosition(latlng);
    speedTest.infoBubble.open(speedTest.map);
  };
};

speedTest.clear = function() {
  $('timetaken').innerHTML = 'cleaning...';
  for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
    marker.setMap(null);
  }
};

speedTest.change = function() {
  speedTest.clear();
  speedTest.showMarkers();
};

speedTest.time = function() {
  $('timetaken').innerHTML = 'timing...';
  var start = new Date();
  if ($('usegmm').checked) {

      var clusterStyles = [
      {
        textColor: 'transparent',
        url: '../img/map/marker.png',
        height: 42,
        width: 32
      },
     {
        textColor: 'transparent',
        url: '../img/map/marker.png',
        height: 42,
        width: 34
      },
     {
        textColor: 'transparent',
        url: '../img/map/marker.png',
        height: 42,
        width: 34
      }
    ];

      var mcOptions = {
                  styles: clusterStyles,
                  };


    speedTest.markerClusterer = new MarkerClusterer(speedTest.map, speedTest.markers, mcOptions);
  } else {
    for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
      marker.setMap(speedTest.map);
    }
  }

  var end = new Date();
  $('timetaken').innerHTML = end - start;
};
