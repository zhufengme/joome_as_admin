<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<body>

<form action="" method="get">
代理商：<select name="clid">
<option value="0"{if ($clid == 0)} selected="selected"{/if}>请选择...</option>
{section loop=$cls_arr name=ls}
<option value="{$cls_arr[ls].CLID}"{if ($clid == "{$cls_arr[ls].CLID}")} selected="selected"{/if}>{$cls_arr[ls].cl_name} - {$cls_arr[ls].CLID} - {$cls_arr[ls].seat_coefficient} - {$cls_arr[ls].seat_num}</option>
{/section}
<!-- <option value="JM-0005"{if ($clid == "JM-0005")} selected="selected"{/if}>Costa Coffee - 东方银座店 - JM-0005</option>
<option value="JM-0003"{if ($clid == "JM-0003")} selected="selected"{/if}>Costa Coffee - 三里屯33店 - JM-0003</option>
<option value="JM-0009"{if ($clid == "JM-0009")} selected="selected"{/if}>院落餐厅 - 地安门店 - JM-0009</option>
<option value="JM-0010"{if ($clid == "JM-0010")} selected="selected"{/if}>院落餐厅 - 太阳宫店 - JM-0010</option>
<option value="JM-0006"{if ($clid == "JM-0006")} selected="selected"{/if}>鱼眼咖啡 - 三里屯店 - JM-0006</option>
<option value="RT5-0020"{if ($clid == "RT5-0020")} selected="selected"{/if}>Costa Coffee - 西单君太店 - RT5-0020 - 先</option>
<option value="JM-0008"{if ($clid == "JM-0008")} selected="selected"{/if}>Costa Coffee - 西单君太店 - JM-0008 - 后</option>
<option value="RT5-0020,JM-0008"{if ($clid == "RT5-0020,JM-0008")} selected="selected"{/if}>Costa Coffee - 西单君太店 - RT5-0020,JM-0008 - 先后</option>
<option value="JM-0012"{if ($clid == "JM-0012")} selected="selected"{/if}>巴伐利亚啤酒坊 - 意大利风情街 - JM-0012 - 合并数据</option>
<option value="JM-0013"{if ($clid == "JM-0013")} selected="selected"{/if}>巴伐利亚啤酒坊 - 意大利风情街 - JM-0013 - 合并数据</option>
<option value="JM-0012,JM-0013"{if ($clid == "JM-0012,JM-0013")} selected="selected"{/if}>巴伐利亚啤酒坊 - 意大利风情街 - JM-0012,JM-0013 - 合并数据</option> -->
</select>
日期：<select name="date">
<option value="0"{if ($date == 0)} selected="selected"{/if}>请选择...</option>
<option value="2012-11-02 00:00:00"{if ($date == "2012-11-02 00:00:00")} selected="selected"{/if}>2012-11-02</option>
<option value="2012-11-03 00:00:00"{if ($date == "2012-11-03 00:00:00")} selected="selected"{/if}>2012-11-03</option>
<option value="2012-11-04 00:00:00"{if ($date == "2012-11-04 00:00:00")} selected="selected"{/if}>2012-11-04</option>
<option value="2012-11-05 00:00:00"{if ($date == "2012-11-05 00:00:00")} selected="selected"{/if}>2012-11-05</option>
<option value="2012-11-06 00:00:00"{if ($date == "2012-11-06 00:00:00")} selected="selected"{/if}>2012-11-06</option>
<option value="2012-11-07 00:00:00"{if ($date == "2012-11-07 00:00:00")} selected="selected"{/if}>2012-11-07</option>
<option value="2012-11-08 00:00:00"{if ($date == "2012-11-08 00:00:00")} selected="selected"{/if}>2012-11-08</option>
</select>

<input type="submit" value="GO"/>
</form>

<br /><br /><br />

<table align="center" border="1" width="900">
	<caption><h4>当前店家</h4></caption>
	<tr>
		<th>统计日期</th>
		<th>CLID</th>
		<th>UID</th>
		<th>cl_name</th>
		<th>timezone</th>
		<th>seat_num</th>
		<th>seat_coefficient</th>
		<th>office_hours</th>
	</tr>
	{section loop=$cls_arr name=ls}
	{if ($cls_arr[ls].CLID == $smarty.get.clid)}
	<tr>
		<td>{$smarty.get.date}</td>
		<td>{$cls_arr[ls].CLID}</td>
		<td>{$cls_arr[ls].UID}</td>
		<td>{$cls_arr[ls].cl_name}</td>
		<td>{$cls_arr[ls].timezone}</td>
		<td>{$cls_arr[ls].seat_num}</td>
		<td>{$cls_arr[ls].seat_coefficient}</td>
		<td>{$cls_arr[ls].office_hours}</td>
	</tr>
	{/if}
	{sectionelse}
	<tr>
		<td colspan="9">没有数据</td>
	</tr>
	{/section}

</table>


<table align="center" border="1" width="900">
	<caption><h4>具体数据</h4></caption>
	<tr>
		<th>停留人次</th>
		<th>平均停留时间（分钟）</th>
		<th>高峰时段</th>
		<th>峰值人数</th>
		<th>新顾客人数</th>
		<th>回头客人数</th>
		<th>座位数</th>
		<th>系数</th>
	</tr>
	{if ($ggg > 0)}
	<tr>
		<td colspan="8">含系数数据</td>
	</tr>
	<tr>
		<td>{ceil($aaa*$ggg)}</td>
		<td>{ceil($bbb)}</td>
		<td>{$ccc}</td>
		<td>{ceil($ddd*$ggg)}</td>
		<td>{ceil($eee*$ggg)}</td>
		<td>{ceil($aaa*$ggg)-ceil($eee*$ggg)}</td>
		<th>{$hhh}</th>
		<th>{$ggg}</th>
	</tr>
	{/if}
	<tr>
		<td colspan="8">原始数据</td>
	</tr>	
	<tr>
		<td>{$aaa}</td>
		<td>{ceil($bbb)}</td>
		<td>{$ccc}</td>
		<td>{$ddd}</td>
		<td>{$eee}</td>
		<td>{$fff}</td>
		<th>{$hhh}</th>
		<th>{$ggg}</th>
	</tr>
</table>

</body>
</html>

