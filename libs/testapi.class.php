<?php
class testapi {
	private $KEY;
	private $api;
	private $ID;
	private $timestamp;
	
	public function __construct($ID, $KEY, $api) {
		$this->KEY = $KEY;
		$this->ID = $ID;
		$this->api = $api;
		$this->timestamp=get_utc_timestamp();
	}

	/**
	 *
	 * 对API请求参数数组进行加密
	 * @param array $arr 请求参数数组
	 *
	 * @return string 加密后的_d;
	 */
	private function api_request_encode($arr){
		$arr['_t']=$this->timestamp;
		$json=json_encode($arr);
		$_d=$this->api_encrypt($json);
		$_d=urlencode($_d);
		return $_d;
	}
	
	
	
	/**
	 *
	 * 对API返回结果解密
	 * @param string $_d 需要解密的字符串
	 *
	 * @return array 解密后的数组;
	 */
	private function api_response_decode($_d){
		if(instr($_d, '%')){
			$_d=urldecode($_d);
		}
		$json=$this->api_decrypt($_d);
		$arr=json_decode($json,true);
		return $arr;
	}
	
	
	private function api_encrypt($encrypt) {
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, $this->PK, $encrypt, MCRYPT_MODE_ECB, $iv );
		$encode = base64_encode ( $passcrypt );
		return $encode;
	}
	
	private function api_decrypt($decrypt) {
		$decoded = base64_decode ( $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $this->PK, $decoded, MCRYPT_MODE_ECB, $iv );
		return trim ( $decrypted );
	}
	
}