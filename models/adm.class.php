<?php
/**
 * 
 * 后台类
 * @author sunwei@joome.net
 *
 */
class adm extends model {

	public function __construct() {
		parent::__construct ();
	}

	/**
	 *
	 *
	 * 登陆验证
	 * 
	 * @param string $acc
	 *			用户名        	
	 * @param string $pw
	 *			密码
	 *
	 */
	public function checkadmin($acc, $pw){
		$db = self::get_db_connect();
		$sql = "select * from proxy_list where proxy_name = '".$acc."' and password = '".md5($pw)."'";
		$array = $db->get_results($sql, ARRAY_A);
		if (count($array) == 1) {
			return $array;
		} else {
			return false;
		}

	}

	/**
	 * 获得代理商列表
	 */
	public function get_proxy_list(){
		$db = self::get_db_connect();
		$array = $db->get_results("SELECT pid,proxy_name FROM proxy_list ORDER BY pid ASC", ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}		
	}	

	/**
	 *
	 *
	 * 对AS添加命令
	 * 
	 * @param array $arr
	 *			传入数组
	 *
	 */
	public function control_insert($arr){
		$db = self::get_db_connect();
		$sql = "insert into nodes_cmd_queue (CLID,CODE,CMD,logtime) values ('".$arr['CLID']."', '".$arr['CODE']."', '".$arr['CMD']."', '".$arr['logtime']."')";
		$ret = $db->query ($sql);
		
		if ($ret) {
			return true;
		} else {
			return false;
		}
	}

	//获取有ip的数量
	public function count_ip_nodes_status(){
		$db = self::get_db_connect();
		$sql = "SELECT count(A_IP) as nums FROM nodes_status where A_IP != ''";
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}	
	}

	//获取状态
	public function get_ip_nodes_status($page){
		$db = self::get_db_connect();
		echo $sql = "SELECT A_IP,CLID FROM nodes_status where A_IP != '' and ISGEO = '' LIMIT ".$page;
		echo "<br>";
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}	


	}

	//更新状态
	public function update_nodes_status($LAT, $LON, $IS_GEO, $CCODE, $CNAME, $RNAME, $CINAME, $TZONE, $CLID){
		//$locations['countryName'], $locations['regionName'], $locations['cityName'], $locations['timeZone']
		$db = self::get_db_connect();
		echo $sql = "UPDATE nodes_status SET LAT = '".$LAT."', LON = '".$LON."',
		 ISGEO = '".$IS_GEO."', CCODE = '".$CCODE."', CNAME = '".$CNAME."',
		  RNAME = '".$RNAME."', CINAME = '".$CINAME."', TZONE = '".$TZONE."', LATLON = '".$LAT.",".$LON."' WHERE CLID = '".$CLID."'";
		echo "<br>";
		$ret = $db->query($sql);
		var_dump($ret);
		echo "<br>";
		if ($ret) {
			return true;
		} else {
			return false;
		}
	}

	//获取全部的坐标信息
	public function get_all_geo(){
		$db = self::get_db_connect();
		$sql = "SELECT LAT,LON FROM nodes_status where ISGEO = '1' and CCODE != 'JP'";//去掉日本的IP
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}			
	}
	//
	public function unique_all_geo(){
		$db = self::get_db_connect();
		$sql = "SELECT count(*) as sum,LAT,LON,CLID,OL FROM nodes_status where ISGEO = '1' and CCODE != 'JP' group by LAT order by LON desc";
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}			
	}	
	public function new_unique_all_geo(){
		$db = self::get_db_connect();
		$sql = "SELECT LAT,LON,ST,OL,CINAME,LAST FROM nodes_status where ISGEO = '1' and CCODE = 'CN' order by CLID desc";
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}			
	}	
	public function status_count($sql){
		$db = self::get_db_connect();
		$array = $db->get_results($sql, ARRAY_A);
		if (!empty($array) && is_array($array)) {
			return $array;
		} else {
			return false;
		}			
	}		




}